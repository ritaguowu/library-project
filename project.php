<?php
ini_set('display errors', 1);
ini_set('display startup errors', 1);
ini_set("log_errors", 1);
ini_set("error_log", "/php-error.log");
//phpinfo();

session_cache_limiter(false);
session_start();

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


//cp4928
//wnom6m4q

if ($_SERVER['SERVER_NAME'] != 'localhost') { // localhost
DB::$user = 'cp4928_libraryaaww';
DB::$password = 'qYmVl3nxl1kz';
DB::$dbName = 'cp4928_libraryaw';
DB::$encoding='utf8';
DB::$host="ipd17.com";
//DB::$port=3333;
}else{
//DB::$user = 'cp4928_libraryaw';
//DB::$password = 'qYmVl3nxl1kz';
//DB::$dbName = 'cp4928_libraryaw';
//Wu Guo's DB
DB::$user = 'libraryaw1';
DB::$password = 'iLbcSkXmhGGqxPJh';
DB::$dbName = 'libraryaw1';
DB::$encoding='utf8';
DB::$port=3333;
}

DB::$error_handler = 'database_error_handler';
DB::$nonsql_error_handler = 'database_error_handler';



function database_error_handler($params) {
    global $app, $log;
    $log->error("SQL Error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query: " . $params['query']);
    }
    $app->render("internal_error.html.twig");
    http_response_code(500);
    die(); // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

\Slim\Route::setDefaultConditions(array(
    'id' => '[1-9][0-9]*'
));

function getUserIpAddr() {
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
require_once 'registerUser.php';

require_once 'admin.php';


$app->get('/', function()  use ($app){
  $app->render('homepage.html.twig');   
});


$app->get('/isemailregistered/(:email)', function($email="") use($app)
{
    //Queryy's first row to find a member with matching email
    $user = db::queryFirstRow("SELECT * FROM members where email=%s",$email);
    if($user)
    {
        //if user is found it echoes email is registered else nothing
        echo "Email already registered";
        $errorList['emailError'] ="";
    }
    else 
    {
        echo "";
    }

});

$app->get("/member/:action(/:id)", function($action, $id=0) use ($app)
{
    //Checks if the session user is set
    //if session user is a regular user, redirects to login page, logged in members cant register
    if($_SESSION['user']['role']=='user')
    {
        $app->redirect('/login');
        return;
    }
    //if the action doesnt match the conditions we've set it sends a not found page
    if(($action=='register'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    //if the user is set to admin and the action equals edit he can edit his account info
    if(isset($_SESSION['user'])&&$_SESSION['user']['role']=='admin'&&$action=='edit')
    {
        
        $roleType=$_SESSION['user']['role'];
        $member = DB::queryFirstRow("SELECT * FROM members where id=%s",$id);
        $app->render('register.html.twig', array ('roleType'=>$roleType, 'v'=>$member, 'action'=>$action));
    }
    //else non logged in users and admins can register memebrs
    else
    {
    $app->render('register.html.twig', array('action'=>$action));
    }
})->conditions(array('action'=>'(register|edit)'));


$app->post("/member/:action(/:id)", function($action, $id=0) use ($app,$log)
{
    //Checks if the session user is set to admin
    if($_SESSION['user']['role']=='user')
    {
        $app->redirect('/login');
        return;
    }
    //if the action doesnt match the conditions we've set it sends a not found page
    if(($action=='register'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    //app request for inputs from a form post
    $firstName=$app->request()->post('firstName');
    
    $lastName=$app->request()->post('lastName');
    
    //checks if the post request expiryDate is set, will only be set for admins
    if(isset($_POST['expiryDate']))
    {
    $expiryDate=$app->request()->post('expiryDate');
    }
    //else the expiry date is current date plus 2 years
    else
    {
        $date=date('Y-m-d');
        $expiryDate= date('Y-m-d', strtotime($date. '+ 2 years'));      
    }
    $email=$app->request()->post('email');
    
    $pass1=$app->request()->post('pass1');
    
    $pass2=$app->request()->post('pass2');
    
    $address=$app->request()->post('address');
    
    $roleType=isset($_SESSION['member'])?$_SESSION['member']['role']:"";

    $role=isset($_POST['role'])?$app->request()->post('role'):"user";
    
    //error list array for validation
    $errorList = array();
   
    //Validation for post inputs for register and edit
    if(!preg_match('/^[a-zA-Z\-]{1,20}$/', $firstName))
    {
        $errorList['firstNameError'] = "First name must be between 1 and 20 characters";
        $firstName="";
    }
    if(!preg_match('/^[a-zA-Z\-]{1,20}$/', $lastName))
    {
        $errorList['lastNameError'] = "Last name must be between 1 and 20 characters";
        $lastName="";
    }
    if($expiryDate==null)
    {
        $errorList['dateError'] = "Date cannot be null";
        $expiryDate="";
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
       $errorList['emailError'] = "Email must contain a '@' and a '.' domain";
       $email="";
    }
    else
    {
        if(strlen($email)<5||strlen($email)>100)
        {
            $errorList['emailError'] = "Email must be between 5 and 100 characters";
            $email="";
        }
        else 
        {
            $user = db::queryFirstRow("SELECT * FROM members where email=%s",$email);
            if($user&&$action!='edit')
            {
                 $errorList['emailError'] = "Email already taken try another ";
            }
        }
    }
    if(strlen($pass1) < 6 ||strlen($pass1) > 50 || (preg_match("/[A-Z]/", $pass1) == FALSE )|| (preg_match("/[a-z]/", $pass1) == FALSE ) || (preg_match("/[0-9]/", $pass1) == FALSE )) 
    {
        $errorList['pass1Error'] = "password must be between 6 and 50 characters and contain at least 1 lower, upper and a digit";
        
    }
    if($pass1!=$pass2)
    {
        $errorList['pass2Error'] = "passwords dont match";
        
    }
    if(strlen($address)<2||strlen($address)>100)
    {
        $errorList['addressError'] = "address must be between 2 and 100 characters";
        $address="";
    }
    
    //if there are errors rendder them 
    if($errorList)
    {
        $app->render('register.html.twig', array('errorList'=>$errorList, 'roleType'=>$roleType, 'pass1'=>$pass1,'pass2'=>$pass2, 'action'=>$action, 'v'=> array('firstName'=>$firstName,'lastName'=>$lastName,'expiryDate'=>$expiryDate,
        'email'=>$email,'address'=>$address, 'role'=>$role)));
    }
    else
    {
        //else if the action is register insert these values into the database
        if($action=='register')
        {
            DB::insert('members', array('firstName'=>$firstName,'lastName'=>$lastName,'expiryDate'=>$expiryDate,
            'email'=>$email, 'password'=>$pass1,'address'=>$address, 'role'=>$role));
            $userId= DB::insertId();
            $log->debug("User registered with id=".$userId);
        }
        else
        {   //else update these values into the database with the matching id     
            DB::update('members', array('firstName'=>$firstName, 'lastName'=>$lastName, 'expiryDate'=>$expiryDate, 'email'=>$email,'password'=>$pass1, 'address'=>$address, 'role'=>$role), 'id=%i', $id);
        }

        
        $app->render('register_success.html.twig');
    }    
});




$app->get("/login", function() use ($app)
{
    //checks if the user is set and if it equals admin redirects to admin homepage
    if (isset($_SESSION['user'])) {
        if($_SESSION['user']['role'] == 'admin')
        {
            $app->redirect('/login/admin');
            return;
        }
        //redirects to user homepage
        else if ($_SESSION['user']['role'] == 'user')
        {
            $app->redirect('/registerUser');
            return;
        }
    }
    //renders the login page
    else
    {
        $app->render('login.html.twig');
    }
});

$app->post("/login", function() use ($app, $log)
{
    //app request for inputs from a form post
    $email=$app->request()->post('email');
    $pass1=$app->request()->post('password');
    
    $errorList = array();
    
    //queries for first row that matches the email of post request
    $member = db::queryFirstRow("SELECT * FROM members WHERE email=%s", $email);
    
    //if it doesnt match validation errors
    if(!$member||$member['password']!=$pass1||!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        array_push($errorList, "Email or password is invalid");
        
    }
    //display error list
    if($errorList)
    {
        //log the unsucessful log in attempt
        $log->info(sprintf("Login failed email=%s, from IP=%s", $email, getUserIpAddr()));
        $app->render('login.html.twig', array('errorList'=>$errorList));
    }
    else
    {
        //succesful login unsets user password sets session for queried member and logs the successful log in
        unset($member['password']);
        $_SESSION['user']=$member;
        $log->info(sprintf("Login successful email=%s, from IP=%s", $email, getUserIpAddr()));
        //
        if($_SESSION['user']['role']=='user')
        {
            $app->redirect('/registerUser');
        }
        else
        {
            $app->redirect('/login/admin');
        }
    }
});


$app->get('/logout', function() use ($app){
    //logout page unsets session of logged in user and redirects to login page
    unset($_SESSION['user']);
    $app->redirect('/');

});

$app->get("/recovery/password", function() use ($app)
{
    //renders the page for recovery/password
    $app->render('recovery/password.html.twig');
});

$app->post("/recovery/password", function() use ($app,$log)
{
    //app request for inputs from a form post
    $email=$app->request()->post('email');
    
    $errorList = array();
    
    //validation for errors
    if($email=="")
    {
        $errorList['emailNull'] = "Email cannot be empty";
        $email = "";
    }
    else
    {
        $member = DB::queryFirstRow("SELECT * FROM members where email=%s",$email);
        if(!$member)
        {
            $errorList['notFound']= "Email was not registered";
        }
    }
    
    //diaplys the error list 
    if($errorList)
    {
        $app->render('recovery/password.html.twig', array('errorList'=>$errorList, 'email'=>$email));
    }
    else
    {
        //unsets the member password, sets session of the member and redirects to the verify step
        unset($member['password']);
        $_SESSION['member']=$member;
        $app->redirect('/recovery/verify');
    }
});

$app->get("/recovery/verify", function() use ($app,$log)
{
    //if session member is not set, redirect to forbidden page
    if(!isset($_SESSION['member']))
    {
    $app->redirect('/forbidden');
    return;
    }
    
    //takes the email from session user explodes it into multiple strings 
    $email = $_SESSION['member']['email'];
    $emailSub1 = explode('@',$email);
    $emailSub2 = explode('.',$emailSub1[1]);
    //grabs a substring 
    $emailLetter1 = substr($emailSub1[0],0,1);
    $emailLetter2 = substr($emailSub1[0],-1,1);
    $emailLetter3 = substr($emailSub1[1],0,1);
    //inserts each substring into a larger string for a starred out email
    $emailEncrypt = $emailLetter1."****".$emailLetter2."@".$emailLetter3."****.com";
    
    $user = $_SESSION['member'];
    $app->render('recovery/verify.html.twig', array('user'=>$user, 'emailEncrypt'=>$emailEncrypt));

});

//function to generate a random string of letters and numbers
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$app->post("/recovery/verify", function() use ($app,$log)
{
    //if session member isnt set, forbidden redirect
    if(!isset($_SESSION['member']))
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //email equals the session member
    $email = $_SESSION['member']['email'];
    
    //reset code equals random string function
    $resetCode = generateRandomString();
    
    //updates the member reset code with ours, where the member matches email
    DB::update("members", array("resetCode"=>$resetCode),"email=%s",$email);
    
    //creates variables for emails to be sent
    $subject = "Password Reset";
    $from = "libraryaw/ipd17.com";
    $msg = "This is you code for your password reset ".$resetCode;
    
    //sends email 
    mail($email,$subject,$msg, $from);

    $app->redirect('/recovery/code');

});

$app->get("/recovery/code", function() use ($app,$log)
{
    //if session member isnt set, forbidden redirect
    if(!isset($_SESSION['member']))
    {
        $app->redirect('/forbidden');
        return;
    }
    $app->render('recovery/code.html.twig');

});

$app->post("/recovery/code", function() use ($app,$log)
{
    //if session member isnt set, forbidden redirect
    if(!isset($_SESSION['member']))
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //grabs the post request for reset code
    $resetCode = $_POST['resetCode'];
    
    $errorList = array();
    
    //queries for users unique reset code
    $user = DB::queryFirstRow("SELECT * FROM members where resetCode=%s", $resetCode);
    
    //if user isnt found render invalid reset code page
    if(!$user)
    {
        array_push($errorList, "Invalid Code");
        $app->render('recovery/code.html.twig', array('errorList'=>$errorList));
    }
    //else redirects to create a new password page
    else
    {
        $app->redirect('/recovery/new');
    }

});

$app->get("/recovery/new", function() use ($app,$log)
{
    //if session member isnt set, forbidden redirect
    if(!isset($_SESSION['member']))
    {
        $app->redirect('/forbidden');
        return;
    }
    $app->render('recovery/new.html.twig');

});

$app->post("/recovery/new", function() use ($app,$log)
{
    //if session member isnt set, forbidden redirect
    if(!isset($_SESSION['member']))
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //app request for password and pssword confirmation
    $pass1=$app->request()->post('pass1');
    $pass2=$app->request()->post('pass2');
    
    $errorList = array();
    
    
    //validates passwords given 
    if(strlen($pass1) < 6 ||strlen($pass1) > 50 || (preg_match("/[A-Z]/", $pass1) == FALSE )|| (preg_match("/[a-z]/", $pass1) == FALSE ) || (preg_match("/[0-9]/", $pass1) == FALSE )) 
    {
        $errorList['pass1Error'] = "password must be between 6 and 50 characters and contain at least 1 lower, upper and a digit";
        
    }
    else if($pass1!=$pass2)
    {
        $errorList['pass2Error'] = "passwords dont match";
        
    }
    
    //displays error page
    if($errorList)
    {
        $app->render('recovery/new.html.twig', array('errorList'=>$errorList));
    }
    else
    {
        //if successful updates member password where email equals session email 
        $email=$_SESSION['member']['email'];
        DB::update("members", array('password'=>$pass1),"email=%s",$email);
        
        //unset session member and render complete page
        unset($_SESSION['member']);
        $app->render('recovery/complete.html.twig');
    }

});




$app->run();


