-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jul 02, 2019 at 06:09 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `libraryaw`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `titleId` int(11) NOT NULL,
  `language` varchar(100) DEFAULT NULL,
  `category` enum('Kids','Cook','Westerns','Computer','Arts&Music') NOT NULL DEFAULT 'Computer',
  `cover` enum('hard','soft','ebook') DEFAULT NULL,
  `loanable` tinyint(1) NOT NULL DEFAULT '1',
  `imagePath` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `ISBN`, `titleId`, `language`, `category`, `cover`, `loanable`, `imagePath`) VALUES
(1, '1001', 1, 'English', 'Kids', 'hard', 0, '/img/book/book8.jfif'),
(2, '1002', 2, 'English', 'Kids', 'hard', 0, '/img/book/book1.jfif'),
(3, '1003', 3, 'English', 'Kids', 'hard', 0, '/img/book/book3.jfif'),
(4, '1004', 4, 'French', 'Kids', 'hard', 1, '/img/book/book6.jfif'),
(5, '1005', 5, 'English', 'Kids', 'hard', 1, '/img/book/book2.jfif'),
(6, '1006', 6, 'French', 'Kids', 'hard', 1, '/img/book/book4.jfif'),
(7, '1007', 7, 'English', 'Kids', 'hard', 1, '/img/book/book5.jfif'),
(8, '1008', 8, 'English', 'Kids', 'hard', 0, '/img/book/book7.jfif');

-- --------------------------------------------------------

--
-- Table structure for table `copies`
--

CREATE TABLE `copies` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `titleId` int(11) NOT NULL,
  `on_Loan` tinyint(1) NOT NULL DEFAULT '1',
  `renew_times` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `copies`
--

INSERT INTO `copies` (`id`, `ISBN`, `titleId`, `on_Loan`, `renew_times`) VALUES
(1, '1001', 1, 1, 2),
(2, '1002', 2, 1, 2),
(3, '1003', 3, 1, 0),
(4, '1004', 4, 0, 0),
(5, '1005', 5, 0, 0),
(6, '1006', 6, 0, 0),
(7, '1007', 7, 0, 0),
(8, '1008', 8, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `copyId` int(11) NOT NULL,
  `titleId` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `outDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dueDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`id`, `ISBN`, `copyId`, `titleId`, `memberId`, `outDate`, `dueDate`) VALUES
(1, '1001', 1, 1, 4, '2019-06-27 01:35:20', '2019-07-14'),
(2, '1002', 2, 2, 4, '2019-06-27 01:35:56', '2019-07-14'),
(3, '1003', 3, 3, 3, '2019-07-01 17:33:58', '2019-07-19'),
(11, '1008', 8, 8, 4, '2019-07-02 04:00:00', '2019-07-16');

-- --------------------------------------------------------

--
-- Table structure for table `loan_history`
--

CREATE TABLE `loan_history` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `outDate` date NOT NULL,
  `titleId` int(11) NOT NULL,
  `MemberId` int(11) NOT NULL,
  `dueDate` date NOT NULL,
  `InDate` date NOT NULL,
  `fineAssesed` int(11) DEFAULT NULL,
  `finePaid` int(11) DEFAULT NULL,
  `remarks` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loan_history`
--

INSERT INTO `loan_history` (`id`, `ISBN`, `outDate`, `titleId`, `MemberId`, `dueDate`, `InDate`, `fineAssesed`, `finePaid`, `remarks`) VALUES
(1, '1003', '2019-06-13', 3, 4, '2019-06-29', '2019-06-29', 12, 10, NULL),
(2, '1004', '2019-06-21', 4, 4, '2019-07-02', '2019-06-27', NULL, 13, NULL),
(3, '1005', '2019-06-11', 4, 4, '2019-06-25', '2019-07-18', 5, 5, NULL),
(5, '1005', '2019-06-10', 5, 4, '2019-06-29', '2019-06-17', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `expiryDate` date NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `role` enum('user','admin') NOT NULL DEFAULT 'user',
  `password` varchar(50) NOT NULL,
  `resetCode` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `lastName`, `firstName`, `expiryDate`, `address`, `email`, `role`, `password`, `resetCode`) VALUES
(3, 'Guo', 'Wu', '2019-06-12', 'zXzxfsdfsdfsdffffffffffffffffffffvcvdsfsdfsdf', 'ritaguowu@gmail.com', 'admin', '', ''),
(4, 'T', 'Peter', '2020-05-20', 'rfsdsdfdsffsdfsdfsdfdsfsdfsfsf', 'aaa@bbb.ccc', 'user', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `memberId` int(11) NOT NULL,
  `logDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiryDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `ISBN`, `memberId`, `logDate`, `expiryDate`) VALUES
(23, '1004', 4, '2019-07-02 03:06:31', '2019-08-01');

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `id` int(11) NOT NULL,
  `titleName` varchar(150) NOT NULL,
  `author` varchar(50) NOT NULL,
  `synopsis` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`id`, `titleName`, `author`, `synopsis`) VALUES
(1, 'A bumblebee sweater', 'Waterton, Betty, LaFave, Kim.', 'When Nellie tells her grandmother that she is going to play the part of a bumblebee in the spring concert, her grandmother knits her a bumblebee-striped sweater. Every time she wears the sweater, Nellie gets into a scrape.'),
(2, 'Dangerous!', 'Warnes, Tim.', 'Mole loves to stick labels on objects. One day, he finds something unusual in the woods. He puts a bunch of labels on the beast—lumpy, bumpy, gigantic. Then the thing frightens Mole, so Mole adds the label “dangerous.” But is the Lumpy-Bumpy Thing really all that dangerous?'),
(3, 'The rainbow fish', 'Pfister, Marcus.', 'The most beautiful fish in the entire ocean discovers the real value of personal beauty and friendship.\r\n'),
(4, 'Also an octopus, or, a little bit of nothing', 'Tokuda-Hall, Maggie. Davies, Benji.', '\"It begins with an octopus who plays the ukulele. Since this is a story, the octopus has to want something--maybe to travel to faraway galaxies in a totally awesome purple spaceship. Then the octopus sets out to build a spaceship out of soda cans, glue, umbrellas, glitter, and waffles. OK, maybe the octopus needs some help, like from an adorable bunny friend, and maybe that bunny turns out to be . . . a rocket scientist? (Probably not.) But could something even more amazing come to pass? Debut author Maggie Tokuda-Hall, with the help of illustrator Benji Davies, sets up an endearingly funny story, then hands the baton to readers, who will be more than primed to take it away.\" --'),
(5, 'Rudolph shines again', 'May, Robert L. Caparo, Antonio Javier,', 'Devastated when his nose no longer shines brightly, Rudolph spends his time worrying, weeping, and feeling sorry for himself until he searches for two baby rabbits lost in the woods.'),
(6, 'Axel Scheffler\'s flip flap safari', 'Scheffler, Axel,', 'Split page format.Level 2'),
(7, 'Mr. Groundhog wants the day off', 'Vojta, Pat Stemper. Levitskiy, Olga,', '	Mr. Groundhog is disappointed when his animal friends refuse to take his place on Groundhog Day, but they help him to have fun forecasting the weather.'),
(8, 'How to be a lion', 'Vere, Ed.', '	\"When Leonard the lion and his friend Marianne, a duck, are confronted by a pack of lion bullies, they find a creative way to stand up for themselves\"--');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ISBN` (`ISBN`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `copies`
--
ALTER TABLE `copies`
  ADD PRIMARY KEY (`id`,`ISBN`),
  ADD KEY `ISBN` (`ISBN`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ISBN` (`ISBN`,`copyId`),
  ADD KEY `copyId` (`copyId`),
  ADD KEY `memberId` (`memberId`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `loan_history`
--
ALTER TABLE `loan_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MemberId` (`MemberId`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ISBN` (`ISBN`),
  ADD KEY `memberId` (`memberId`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `copies`
--
ALTER TABLE `copies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `loan_history`
--
ALTER TABLE `loan_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`);

--
-- Constraints for table `copies`
--
ALTER TABLE `copies`
  ADD CONSTRAINT `copies_ibfk_1` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`),
  ADD CONSTRAINT `copies_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `books` (`ISBN`);

--
-- Constraints for table `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_ibfk_1` FOREIGN KEY (`copyId`) REFERENCES `copies` (`id`),
  ADD CONSTRAINT `loans_ibfk_2` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `loans_ibfk_3` FOREIGN KEY (`ISBN`) REFERENCES `books` (`ISBN`),
  ADD CONSTRAINT `loans_ibfk_4` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`);

--
-- Constraints for table `loan_history`
--
ALTER TABLE `loan_history`
  ADD CONSTRAINT `loan_history_ibfk_1` FOREIGN KEY (`MemberId`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `loan_history_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `books` (`ISBN`),
  ADD CONSTRAINT `loan_history_ibfk_3` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`),
  ADD CONSTRAINT `loan_history_ibfk_4` FOREIGN KEY (`MemberId`) REFERENCES `members` (`id`);

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `reservations_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `books` (`ISBN`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
