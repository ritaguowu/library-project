2019/06/21 Wu Guo
1. What I have done / not done since last Scrum?
- created project proposal(Half)
- created 7 database projects tables on the ipd17.com\cpanel.

2. What I plan to do from this scrum to next? (next 24 hours)
-create Homepage of project and master.html.twig file.

3. Where do I need assistance? What do I need to figure out?
- No, I don't think so.

2019/06/21 Aaron Graham
1. ~~
2.Create login, register, password recovery, search list
3. how to send emails through php, 

2019/06/22 Wu Guo
1. What I have done / not done since last Scrum?
- create Homepage of project
- create master.html.twig file

2. What I plan to do from this scrum to next? (next 24 hours)
-create registered user page
-implement user to check his/her current loan from DB

3. Where do I need assistance? What do I need to figure out?
- No.Thank you.

2019/06/23 Wu Guo
1. What I have done / not done since last Scrum?
- create Homepage of registered user
- Add sidebar to the homepage.

2. What I plan to do from this scrum to next? (next 24 hours)
- Change the content as per the sidebar link.
-implement user to check his/her current loan from DB

3. Where do I need assistance? What do I need to figure out?
- No.Thank you.


2019/06/24 Wu Guo
1. What I have done / not done since last Scrum?
- create a new siderbar.
- Add loan&Renew content.
- Implement renew function mostly. There still have something wrong. 

2. What I plan to do from this scrum to next? (next 24 hours)
- Continue working on renew books
- Implement check loan and payment history function.

3. Where do I need assistance? What do I need to figure out?
- I am not sure. Maybe I can try a while.

2019/06/25 Aaron Graham
1. Create login, register, password recovery, CRUD for admin login
2. CRUD for adding book, 
3. uploading and displaying images through database.

2019/06/25 Wu Guo
1. What I have done / not done since last Scrum?
- Implement renew function  
- Implement check loans order by title name and due date(asc and desc) separately.

2. What I plan to do from this scrum to next? (next 24 hours)
- Implement check loan and payment history function.
- Implement reservation function.


2019/06/26 Aaron Graham
1. Create add entry for titles, books and copies
2. create update and delete for titles, books, copies and members, css
3. ajax loading of titles inputs in book_addedit template


2019/06/26 Wu Guo
1. What I have done / not done since last Scrum?
- Implement check loan history function.

2. What I plan to do from this scrum to next? (next 24 hours)
- Implement login-user can see the detail of specific book.
- Implement reservation function.

2019/06/27 Aaron Graham
1. create update and delete for titles, books, copies 
2. added ajax to crud for book,titles add, css
3. ajax loading of titles inputs in book_addedit template

2019/06/27 Wu Guo
1. What I have done / not done since last Scrum?
- Implement loan history pagination.

2. What I plan to do from this scrum to next? (next 24 hours)
- Implement login-user can see the detail of specific book.
- Implement reservation function.


2019/06/28 Aaron Graham
1. added ajax to crud for book,titles add, css
2. fix update delete members, fix reset password
3. hashcode with database, cookies handling, emailing

2019/06/28 Wu Guo
1. What I have done / not done since last Scrum?
- Fix some problems of our pages, such as: navigator bar,page number position, show text on the order by button in registerUser interface.
-Update the renew and load history page using AJAX technique.

2. What I plan to do from this scrum to next? (next 24 hours)
- Implement login-user can see the detail of specific book.
- Implement reservation function.

2019/06/29 Wu Guo
1. What I have done / not done since last Scrum?
- Implemented login-user can see the detail of specific book.
- Implementing user can search books as per the specific conditions, not done yet.

2. What I plan to do from this scrum to next? (next 24 hours)
- Continue to implement user can search books as per the specific conditions
- Implement reservation function.


2019/06/30 Aaron Graham
1. fix update delete members, fix reset password
2. admin can process a return of a book, more css touch ups 
3. hashcode with database, cookies handling, emailing

2019/06/30 Wu Guo
1. What I have done / not done since last Scrum?
- Implemented login-user can see the detail of specific book.
- Implementing user can search books as per the specific conditions, not done yet.

2. What I plan to do from this scrum to next? (next 24 hours)
- Continue to implement user can search books as per the specific conditions
- Implement reservation function.

2019/07/1 Aaron Graham
1. admin can process a return of a book, more css touch ups
2. admin can view charts of average loans of day,month,year, user can navigate through site more, more css touch ups
3. hashcode with database, cookies handling, emailing

2019/7/1 Wu Guo
1. What I have done / not done since last Scrum?
- implement user can search books as per the specific conditions
- Implement reservation function.
- Implement login-user can borrow book
- Implement login-user can check payment history

2. What I plan to do from this scrum to next? (next 24 hours)
- Do final powerpoint file.

2019/07/2 Aaron Graham
1. admin can process a return of a book, more css touch ups
2. admin can process a return redone, bug fixes, touch ups, power point slides
3. ---

2019/7/2 Wu Guo
1. What I have done / not done since last Scrum?
- Fix almost all the bugs 
- Prepare for the presentation.

2. What I plan to do from this scrum to next? (next 24 hours)
- Continue to fix bugs. 
- Submit the final project.

2019/7/3 Wu Guo
1. What I have done / not done since last Scrum?
- Fix the rest bugs, all the code works very well on localhost, but still have some problems on the server.

2. What I plan to do from this scrum to next? (next 24 hours)
- Continue to fix the server problems