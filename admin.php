<?php

//echo "<pre>"; print_r($book); exit;

if (false)
{
    $app = new \Slim\slim();
}

$app->get("/login/admin", function() use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['admin'])
    {
        $app->redirect('/forbidden');
        return;
    }
    $app->render('admin/admin_master.html.twig', array('v'=>$_SESSION['user']));
});

$app->get('/ajax/title/:title', function($title="") use($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    //queries to check if the title matches a title in the databse
    $titleFound = db::queryFirstRow("SELECT * FROM titles where titleName=%s",$title);
    
    //if found it displays the title table info on page setting them to read only
    if($titleFound)
    {
        $readOnly = 'readonly';
        echo "<h3 style=color:red;>*This title Exists</h3>";
        $app->render('/admin/ajax_title.html.twig', array('t'=>$titleFound, 'r'=>array('readOnly'=>$readOnly)));
    }
    // else displays empty inputs for title information
    else 
    {
        echo "<h3 style=color:blue;>*Title not found</h3>";
        $app->render('/admin/ajax_title.html.twig');     
    }

});

$app->get("/admin/books/:action(/:id)", function($action, $id=0) use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])||$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //checks if action and id meet our conditions
    if(($action=='add'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    
    //if action equals add display empty add page 
    if($action=='add')
    {
    $app->render('admin/books_addedit.html.twig');
    }
    
    //if action equals edit it queries the first book where the id's match
    else
    {
        $book = db::queryFirstRow("SELECT * FROM books AS b INNER JOIN titles AS t ON b.titleId = t.id WHERE b.id=%i", $id);

        //if book not found 
        if(!$book)
        {
            $app->notFound();
            return;
        }
        $app->render("admin/books_addedit.html.twig", array('v'=>$book,'action'=>$action));
    }
})->conditions(array('action'=>'(add|edit)'));



$app->post('/admin/books/:action(/:id)', function($action, $id = 0) use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])||$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //checks if action and id meet our conditions
    if(($action=='add'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    
    //app request for posts 
    $isbn=$app->request()->post('isbn');
    $title=$app->request()->post('title');
    $language=$app->request()->post('language');
    $category=$app->request()->post('category');
    $cover=$app->request()->post('cover');
    $loanable=$cover=='ebook'?0:1;
    $author=$app->request()->post('author');
    $synopsis=$app->request()->post('synopsis');
    
    $errorList = array();
    
    //validating for errors 
    if(!preg_match("/^\d{10}(\d{3})?$/",$isbn))
    {
        $errorList['isbn'] = "isbn must be between 10 and 13 digits ";
        $isbn="";
    }
    if(strlen($title)<1||strlen($title)>150) 
    {
        $errorList['title'] = "title must be between 1 and 150 characters";
        
    }
    if(strlen($language)<1||strlen($language)>100)
    {
        $errorList['language'] ="language must be between 1 and 100 characters";
        $language="";
    } 
    if(strlen($author)<1||strlen($author)>50)
    {
        $errorList['author'] = "author must be between 1 and 50 characters";
        $author="";
    }if(strlen($synopsis)<10||strlen($synopsis)>2000)
    {
        $errorList['synopsis'] ="synopsis must be between 10 and 2000 characters";
        $synopsis="";
    }
    
    //validating for uploading images
    if(isset($_FILES['imagePath']))
    {
        $productImage = $_FILES['imagePath'];
//        echo "<pre>"; print_r($productImage); exit;


        if($productImage['error']!=0)
        {
            array_push($errorList, "File submission failed, make sure you've selected one (1)");

        }
        else
        {
            $data = getimagesize($productImage['tmp_name']);
            if($data == FALSE)
            {
                array_push($errorList, "File submission failed, make sure you've selected an image (2)");
            }
            else
            {
                if(!in_array($data['mime'],array("image/jpeg", 'image/gif', 'image/png')))
                {
                    array_push($errorList, "File submission failed, make sure you've selected jpg/gif/png (3)");
                }
                else
                {
                     // FIXME: sanitize file name, otherwise a security hole, maybe
                $productImage['name'] = strtolower($productImage['name']);
                if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $productImage['name'])) {
                    array_push($errorList, "File submission failed, make sure you've selected an image (4)");
                }
                $productImage['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $productImage['name']);
                if (file_exists('uploads/' . $productImage['name'])) {
                    // array_push($errorList, "File submission failed, refusing to override existing file (5)");
                    $num = 1;
                    $info = pathinfo($productImage['name']);
                    while (file_exists('uploads/' . $info['filename'] . "_$num." . $info['extension'])) {
                        $num++;
                    }
                    $productImage['name'] = $info['filename'] . "_$num." . $info['extension'];
                }
                }
            }
        }
    }
    
    //if error list display error page
    if($errorList)
    {
        $app->render('admin/books_addedit.html.twig', array('errorList'=>$errorList, 'v'=> array('id'=>$id,'ISBN'=>$isbn,'titleName'=>$title, 
            'language'=>$language, 'category'=>$category, 'cover'=>$cover, 'author'=>$author, 'synopsis'=>$synopsis))); 
    }
   
    else
    {
        //creates a string for the image to upload to a path 
        $imagePath = 'uploads/'.$productImage['name'];
        
        //if moving the image fails log the error and redirect to internal error page
        if(!move_uploaded_file($productImage['tmp_name'], $imagePath))
        {
            $log->error("Error moving upload file: ".print_r($productImage, true));
            $app->redirect('/internalerror');
        }
        
        //queries the first row for a book that matches title
        $bookTitle = DB::queryFirstRow("SELECT * FROM titles where titleName=%s",$title);
        
        //if action is add
        if($action=='add')
        {
            //begin transaction
            DB::startTransaction();
            
            //if book title not found 
            if(!$bookTitle)
            {
                //insert that title into our databse if error inserting roll back and display internal error
                $result = DB::insert("titles", array('titleName'=>$title, 'author'=>$author, 'synopsis'=>$synopsis));
                if (!$result) 
                {
                    DB::rollback();
                    $app->redirect('/internalerror');
                } 
            }  
            
             //insert that book into our databse if error inserting roll back and display internal error
            $result = DB::insert('books', array('ISBN'=>$isbn, 'titleId'=>$bookTitle['id'], 'language'=>$language, 'category'=>$category,'cover'=>$cover, 'loanable'=>$loanable, 'imagePath'=>$imagePath));
            if (!$result) 
            {
                DB::rollback();
                $app->redirect('/internalerror');
            } 
            else 
            { 
                 //insert a copy of the book into our databse if error inserting roll back and display internal error
                $result = DB::insert('copies', array('ISBN'=>$isbn, 'titleId'=>$bookTitle['id'], 'on_Loan'=>0, 'renew_times'=>0));
                if (!$result) 
                {
                    DB::rollback();
                    $app->redirect('/internalerror');
                }
            }
            //commit the transaction
            DB::commit();
            
            $app->render('admin/books_addedit_success.html.twig', array('book'=>$bookTitle));
        }
        else
        {
            //update the book where book id matches id
            DB::update('books', array('ISBN'=>$isbn, 'titleId'=>$bookTitle['id'], 'language'=>$language, 'category'=>$category,'cover'=>$cover, 'loanable'=>$loanable, 'imagePath'=>$imagePath), 'id=%i', $id);
            $app->render('admin/books_addedit_success.html.twig',array('action' => $id, 'book'=>$bookTitle));
        }
        
    } 
})->conditions(array('action' => '(add|edit)'));

$app->get('/admin/books/delete/:id', function($id) use ($app, $log) {
    
    //checks if the user is set to admin
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    
    //queries to find item where id's match
    $item = DB::queryFirstRow("SELECT * FROM books WHERE id=%i", $id);
    
    //if item not found display nto found
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->render('admin/books_delete.html.twig', array('item' => $item));
});

$app->post('/admin/books/delete/:id', function($id) use ($app, $log) {
    
    //checks if the user is set to admin
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    
    //querires first row where book id matches id
    $book = DB::queryFirstRow("SELECT * FROM books where id=%s", $id);
    
    $errorList = array();
    
    $loanCheck = DB::query('SELECT * from loans where ISBN=%s', $book['ISBN']);
    $loanHistCheck = DB::query('SELECT * from loans where ISBN=%s', $book['ISBN']);
    if($loanCheck||$loanHistCheck)
    {
        array_push($errorList, "Data deletion of books with loans or loan history is fatal, cannot delete");
    }
    if($errorList)
    {
        $app->render('admin/books_delete.html.twig', array('item' => $book, 'errorList'=>$errorList));
    }
    else
    {
        //if the app reqeuest for post equals true
        if ($app->request()->post('confirmed') == 'true') {

            //we start transaction 
            db::startTransaction();

            //we delete all copies with the same isbn as the book, if no matchh is found we roll back
            $result = DB::delete("copies", "ISBN=%i", $book['ISBN']);
            if(!$result)
            {
                DB::rollback();
                $app->redirect('/internalerror');
            }
            else
            {
                //than we delete all the books that match the id, if no match we role back
                $result = DB::delete("books", "id=%i", $id);
                if(!$result)
                {
                    DB::rollback();
                    $app->redirect('/internalerror');
                }
            }

            //we commit and render the success page
            DB::commit();
            $app->render('admin/books_delete_success.html.twig');
            //else if errors we display internal error page
        } else {
            $app->redirect('/internalerror');
            return;
        }
    }
});

$app->get("/admin/copies/:action(/:id)", function($action, $id=0) use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])||$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    //if action and id dont meet our condition page not found
    if(($action=='add'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    //if action equals add
    if($action=='add')
    {
        $app->render('admin/copies_addedit.html.twig');
    }
    else
    {
        //queries first row from titles where book title equals title
        $copy = db::queryFirstRow("SELECT * FROM copies WHERE id=%i", $id);
        
        //if not found display page not found
        if(!$copy )
        {
            $app->notFound();
            return;
        }
        $app->render("admin/copies_addedit.html.twig", array('v'=>$copy ,'action'=>$action));
    }
})->conditions(array('action'=>'(add|edit)'));

$app->post('/admin/copies/:action(/:id)', function($action, $id = 0) use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])||$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //if action and id dont meet our condition page not found
    if(($action=='add'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    
    //app request for the posts page
    $ISBN=$app->request()->post('ISBN');
    $titleId=$app->request()->post('titleId');
    
    $errorList = array();
    
    //validation for any errors
    //validating for errors 
    if(!preg_match("/^\d{10}(\d{3})?$/",$ISBN))
    {
        $errorList['isbn'] = "isbn must be between 10 and 13 digits ";
        $ISBN="";
    }
    
    $isbnCheck = DB::query('SELECT * FROM books where ISBN=%s',$ISBN);
    if(!$isbnCheck)
    {
        $errorList['isbn'] = "ISBN does not exist, cannot create copy for book that does not exist.";
        $ISBN="";
    }
    if(!preg_match("/^\d{1,10}$/",$titleId))
    {
        $errorList['isbn'] = "title Id must be a digit between 1 and 999999 ";
        $titleId="";
    }
    $titleCheck = DB::query('SELECT * FROM titles where id=%s',$titleId);
    if(!$titleCheck)
    {
        $errorList['isbn'] = "Title ID does not exist, cannot create copy for title that does not exist.";
        $ISBN="";
    }
    
    //displays error list
    if($errorList)
    {
        $app->render('admin/titles_addedit.html.twig', array('errorList'=>$errorList, 'v'=> array('id'=>$id,'ISBN'=>$ISBN, 'titleId'=>$titleId)));
    }
    else
    {      
        //if action equals add insert into titles 
        if($action=='add')
        {
            DB::insert("copies", array('ISBN'=>$ISBN, 'titleId'=>$titleId, 'on_Loan'=>0, 'renew_times'=>0));

            $app->render('admin/titles_addedit_success.html.twig',array('action' => $action));
        }
        
        //update into titles table
        else
        {         
            DB::update('titles', array('ISBN'=>$ISBN, 'titleId'=>$titleId), 'id=%i', $id);
            $app->render('admin/titles_addedit_success.html.twig',array('action' => $action));
        }     
    } 
})->conditions(array('action' => '(add|edit)'));

$app->get('/admin/copies/delete/:id', function($id) use ($app, $log) {
    
    //checks if the user is set to admin
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    
    //queries to find item where id's match
    $item = DB::queryFirstRow("SELECT * FROM copies WHERE id=%i", $id);
    
    //if item not found, display not found page
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->render('admin/copies_delete.html.twig', array('item' => $item));
});

$app->post('/admin/copies/delete/:id', function($id) use ($app, $log) {
    
    //checks if the user is set to admin
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    
    //querires first row where book id matches id
    $copy = DB::queryFirstRow("SELECT * FROM copies where id=%s", $id);
    
    $errorList = array();
    
    $loanCheck = DB::query('SELECT * from loans where copyId=%s', $copy['id']);
    $loanHistCheck = DB::query('SELECT * from loans where copyId=%s', $copy['id']);
    if($loanCheck||$loanHistCheck)
    {
        array_push($errorList, "Data deletion of copies with loans or loan history is fatal, cannot delete");
    }
    if($errorList)
    {
        $app->render('admin/copies_delete.html.twig', array('item' => $copy, 'errorList'=>$errorList));
    }
    else
    {
    
        //if the app reqeuest for post equals true
        if ($app->request()->post('confirmed') == 'true') {


        //we delete the copy where copy id matches id
            DB::delete("copies", "id=%i", $id);

            $app->render('admin/copies_delete_success.html.twig');

            //else if errors we display internal error page
        } else {
            $app->redirect('/internalerror');
            return;
        }
    }
});

$app->get('/admin/titles/delete/:id', function($id) use ($app, $log) {
    
    //checks if the user is set to admin
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    
    //queries to find item where id's match
    $item = DB::queryFirstRow("SELECT * FROM titles WHERE id=%i", $id);
    
    //if item not found, display not found page
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->render('admin/titles_delete.html.twig', array('item' => $item));
});

$app->post('/admin/titles/delete/:id', function($id) use ($app, $log) {
    
    //checks if the user is set to admin
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    
    
     $title = DB::queryFirstRow("SELECT * FROM titles where id=%s", $id);
    
    $errorList = array();
    
    $loanCheck = DB::query('SELECT * from loans where titleId=%s', $title['id']);
    $loanHistCheck = DB::query('SELECT * from loans where titleId=%s', $title['id']);
    if($loanCheck||$loanHistCheck)
    {
        array_push($errorList, "Data deletion of titles with loans or loan history is fatal, cannot delete");
    }
    if($errorList)
    {
        $app->render('admin/titles_delete.html.twig', array('item' => $title, 'errorList'=>$errorList));
    }
    else
    {
        //if the app reqeuest for post equals true
        if ($app->request()->post('confirmed') == 'true') 
        {
            //we start trnasction 
            DB::startTransaction();

            //we delete all copies with the same isbn as the book, if no matchh is found we roll back
            $result = DB::delete("books", "titleId=%s", $id);
            if(!$result)
            {
                DB::rollback();
                $app->redirect('/internalerror');
            }
            else
            {
                $result = DB::delete("copies", "titleId=%s", $id);
                if(!$result)
                {
                    DB::rollback();
                    $app->redirect('/internalerror');
                }

            }
            $result = DB::delete("titles", "id=%s", $id);
            if(!$result)
            {
                DB::rollback();
                $app->redirect('/internalerror');
            }
            else
            {
                DB::commit();
                $app->render('admin/books_delete_success.html.twig');
            }

        } 
        else {
            $app->redirect('/internalerror');
            return;
        }  
    }
});

$app->get("/admin/titles/:action(/:id)", function($action, $id=0) use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])||$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    //if action and id dont meet our condition page not found
    if(($action=='add'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    //if action equals add
    if($action=='add')
    {
        $app->render('admin/titles_addedit.html.twig');
    }
    else
    {
        //queries first row from titles where book title equals title
        $bookTitle = db::queryFirstRow("SELECT * FROM titles WHERE id=%i", $id);
        
        //if not found display page not found
        if(!$bookTitle )
        {
            $app->notFound();
            return;
        }
        $app->render("admin/titles_addedit.html.twig", array('v'=>$bookTitle ,'action'=>$action));
    }
})->conditions(array('action'=>'(add|edit)'));

$app->post('/admin/titles/:action(/:id)', function($action, $id = 0) use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])||$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //if action and id dont meet our condition page not found
    if(($action=='add'&&$id!=0)||($action=='edit'&&$id==0))
    {
        $app->notFound();
        return;
    }
    
    //app request for the posts page
    $titleName=$app->request()->post('titleName');
    $author=$app->request()->post('author');
    $synopsis=$app->request()->post('synopsis');
    
    $errorList = array();
    
    //validation for any errors
    if(strlen($titleName)<1||strlen($titleName)>150) 
    {
        $errorList['titleName'] = "title must be between 1 and 150 characters";
        $titleName="";
        
    }
    $title = DB::queryFirstRow("SELECT * FROM titles where titleName=%s",$titleName);
    if($title&&$action=='add')
    {
        array_push($errorList, "Title Name already exists in database");
        $titleName="";
    }
    if(!preg_match("/^[A-Za-z-\. ,]{2,50}$/",$author))
    {
        $errorList['author'] = "Author name can only contain upper and lower case numbers and -'s and between 2 and 50 characters";
        $author="";
    }
    if(strlen($synopsis)<10||strlen($synopsis)>2000)
    {
        $errorList['synopsis'] ="synopsis must be between 10 and 2000 characters";
        $synopsis="";
    }
    
    
    //displays error list
    if($errorList)
    {
        $app->render('admin/titles_addedit.html.twig', array('errorList'=>$errorList, 'v'=> array('id'=>$id,'titleName'=>$titleName, 'author'=>$author, 'synopsis'=>$synopsis)));
    }
    else
    {      
        //if action equals add insert into titles 
        if($action=='add')
        {
            DB::insert("titles", array('titleName'=>$titleName, 'author'=>$author, 'synopsis'=>$synopsis));

            $app->render('admin/titles_addedit_success.html.twig',array('action' => $action));
        }
        
        //update into titles table
        else
        {         
            DB::update('titles', array('titleName'=>$title, 'author'=>$author, 'synopsis'=>$synopsis), 'id=%i', $id);
            $app->render('admin/titles_addedit_success.html.twig',array('action' => $action));
        }     
    } 
})->conditions(array('action' => '(add|edit)'));


$app->get('/admin/member/delete/:id', function($id) use ($app, $log) {
    
    //checks if admin user is set, only admins can delete memebers
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    //queries for row matching id 
    $member = DB::queryFirstRow("SELECT * FROM members WHERE id=%i", $id);
    if (!$member) {
        $app->notFound();
        return;
    }
    $app->render('admin/member_delete.html.twig', array('m' => $member));
});

$app->post('/admin/member/delete/:id', function($id) use ($app, $log) {
    
    //checks if admin user is set, only admins can delete memebers
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
     $member = DB::queryFirstRow("SELECT * FROM members where id=%s", $id);
    
    $errorList = array();
    
    $loanCheck = DB::query('SELECT * from loans where memberId=%s', $member['id']);
    $loanHistCheck = DB::query('SELECT * from loans where MemberId=%s', $member['id']);
    if($loanCheck||$loanHistCheck)
    {
        array_push($errorList, "Data deletion of titles with loans or loan history is fatal, cannot delete");
    }
    if($errorList)
    {
        $app->render('admin/member_delete.html.twig', array('item' => $member, 'errorList'=>$errorList));
    }
    else
    {
        //if the post request equals true delete the member
        if ($app->request()->post('confirmed') == 'true') {
            DB::delete("members", "id=%i", $id);

            //render the successful delete page
            $app->render('admin/member_delete_success.html.twig');
        } else {
            //if it doesnt work display internal error
            $app->redirect('/internalerror');
            return;
        }
    }
});

$app->get("/admin/members/list", function() use ($app)
 {
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    //gather products per page and total pages to be displayed
    $prodPerPage = 2;
    $memberCount = DB::queryFirstField("SELECT COUNT(*) FROM members");
    $totalPages = max(1,($memberCount + $prodPerPage - 1 ) / $prodPerPage);
    
    if(!$memberCount)
    {
        $app->notFound();
        return;
    }
    $app->render("admin/members_list.html.twig", array('sessionUser' => @$_SESSION['member'],'totalPages' => $totalPages));

 });
 
$app->get('/ajax/members/page/:page(/sortby/:order)', function($page, $order = 'id') use ($app, $log) {
    
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //products per page and querires to count the products
    $prodPerPage = 2;
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM members");
    
    //calcaulates it the page*prodperpage is greater thean prodCount and product per page - 1
    if ($page * $prodPerPage > ($prodCount + $prodPerPage - 1)) {
        $app->notFound();
        return;
    }
    $skip = ($page - 1 ) * $prodPerPage;
    $itemsList = DB::query("SELECT * FROM members ORDER BY %l LIMIT %l,%l", $order, $skip, $prodPerPage);
    $app->render('admin/ajax_members_list.html.twig', array(
        'itemsList' => $itemsList));
    //print_r($itemsList);
    // $log->d('items: ' . print_r($itemsList, true));    
})->conditions(array('page' => '[1-9][0-9]*', 'order' => '(id|name|price)'));


$app->get("/admin/books/list", function() use ($app)
 {
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }

    //gather products per page and total pages to be displayed
    $prodPerPage = 2;
    $bookCount = DB::queryFirstField("SELECT COUNT(*) FROM books");
    $totalPages = max(1,($bookCount + $prodPerPage - 1 ) / $prodPerPage);


    if(!$bookCount)
    {
        $app->notFound();
        return;
    }
    $app->render("admin/books_list.html.twig", array('sessionUser' => @$_SESSION['member'],'totalPages' => $totalPages));

});
 
 $app->get('/ajax/books/page/:page(/sortby/:order)', function($page, $order = 'id') use ($app, $log) {
     
     //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //products per page and querires to count the products
    $prodPerPage = 2;
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM books");
    if ($page * $prodPerPage > ($prodCount + $prodPerPage - 1)) { // TODO: make sure it's right
        $app->notFound();
        return;
    }
    
    $skip = ($page - 1 ) * $prodPerPage;
    
    $itemsList = DB::query("SELECT * FROM books ORDER BY %l LIMIT %l,%l", $order, $skip, $prodPerPage);
    
    $app->render('admin/ajax_books_list.html.twig', array(
        'itemsList' => $itemsList));
    //print_r($itemsList);
    // $log->d('items: ' . print_r($itemsList, true));    
})->conditions(array('page' => '[1-9][0-9]*', 'order' => '(id|name|price)'));

$app->get("/admin/books/list", function() use ($app)
 {
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }

    $prodPerPage = 2;
    $bookCount = DB::queryFirstField("SELECT COUNT(*) FROM books");
    $totalPages = max(1,($bookCount + $prodPerPage - 1 ) / $prodPerPage);


    if(!$bookCount)
    {
        $app->notFound();
        return;
    }
    $app->render("admin/books_list.html.twig", array('sessionUser' => @$_SESSION['member'],'totalPages' => $totalPages));

 });
 
 $app->get("/admin/copies/list", function() use ($app)
 {
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    //gather products per page and total pages to be displayed
    $prodPerPage = 2;
    $memberCount = DB::queryFirstField("SELECT COUNT(*) FROM copies");
    $totalPages = max(1,($memberCount + $prodPerPage - 1 ) / $prodPerPage);
    
    if(!$memberCount)
    {
        $app->notFound();
        return;
    }
    $app->render("admin/copies_list.html.twig", array('sessionUser' => @$_SESSION['member'],'totalPages' => $totalPages));

 });
 
$app->get('/ajax/copies/page/:page(/sortby/:order)', function($page, $order = 'id') use ($app, $log) {
    
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    //products per page and querires to count the products
    $prodPerPage = 2;
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM copies");
    
    //calcaulates it the page*prodperpage is greater thean prodCount and product per page - 1
    if ($page * $prodPerPage > ($prodCount + $prodPerPage - 1)) {
        $app->notFound();
        return;
    }
    $skip = ($page - 1 ) * $prodPerPage;
    $itemsList = DB::query("SELECT * FROM copies ORDER BY %l LIMIT %l,%l", $order, $skip, $prodPerPage);
    $app->render('admin/ajax_copies_list.html.twig', array(
        'itemsList' => $itemsList));
    //print_r($itemsList);
    // $log->d('items: ' . print_r($itemsList, true));    
})->conditions(array('page' => '[1-9][0-9]*', 'order' => '(id|name|price)'));

 
 $app->get("/admin/titles/list", function() use ($app)
 {
     //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }

    $prodPerPage = 2;
    $titleCount = DB::queryFirstField("SELECT COUNT(*) FROM titles");
    $totalPages = max(1,($titleCount + $prodPerPage - 1 ) / $prodPerPage);


    if(!$titleCount)
    {
        $app->notFound();
        return;
    }
    $app->render("admin/titles_list.html.twig", array('sessionUser' => @$_SESSION['member'],'totalPages' => $totalPages));

 });
 
$app->get('/ajax/titles/page/:page(/sortby/:order)', function($page, $order = 'id') use ($app, $log) {
    
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    
    $prodPerPage = 2;
    $prodCount = DB::queryFirstField("SELECT COUNT(*) FROM titles");
    
    if ($page * $prodPerPage > ($prodCount + $prodPerPage - 1)) { // TODO: make sure it's right
        $app->notFound();
        return;
    }
    
    $skip = ($page - 1 ) * $prodPerPage;
    $itemsList = DB::query("SELECT * FROM titles ORDER BY %l LIMIT %l,%l", $order, $skip, $prodPerPage);
    $app->render('admin/ajax_titles_list.html.twig', array(
        'itemsList' => $itemsList));
    
    //print_r($itemsList);
    // $log->d('items: ' . print_r($itemsList, true));    
})->conditions(array('page' => '[1-9][0-9]*', 'order' => '(id|name|price)'));

$app->get("/admin/titles/list", function() use ($app)
 {
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }

    $prodPerPage = 2;
    $titleCount = DB::queryFirstField("SELECT COUNT(*) FROM books");
    $totalPages = max(1,($titleCount + $prodPerPage - 1 ) / $prodPerPage);


    if(!$titleCount)
    {
        $app->notFound();
        return;
    }
    $app->render("admin/titles_list.html.twig", array('sessionUser' => @$_SESSION['member'],'totalPages' => $totalPages));

 });


$app->get("/admin/report", function() use ($app)
{
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }

 
    $app->render("admin/report_year.html.twig");

 });
 
$app->get("/ajax/admin/report/:action", function($action) use ($app)
{
    
    //checks if the user is set to admin
    if(!isset($_SESSION['user'])&&$_SESSION['user']['role']!='admin')
    {
        $app->redirect('/forbidden');
        return;
    }
    //if the action is year queries by year
    if($action=='year')
    {
        $report = DB::query('SELECT Count(ISBN) as loansPer, year(outDate) as dateOrder FROM loans GROUP BY year(outDate) DESC LIMIT 5');
    }
     //if the action is month queries by month
    else if($action=='month')
    {
        $report = DB::query('SELECT Count(ISBN) as loansPer, month(outDate) as dateOrder FROM loans GROUP BY month(outDate)DESC LIMIT 12');     
    }
     //if the action is say queries by day
    else if($action=='day')
    {
        $report = DB::query('SELECT Count(ISBN) as loansPer, day(outDate) as dateOrder FROM loans GROUP BY day(outDate) DESC LIMIT 30');
    }
    echo json_encode($report);


 })->conditions(array('action' => '(year|month|day)'));
 
$app->get('/admin/books/return', function() use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    $app->render('admin/return_book.html.twig');
});

$app->post('/admin/books/return', function() use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }
    
    //app request for post
    $email=$app->request()->post('email');
    $copyId=$app->request()->post('copyId');
    $ISBN=$app->request()->post('ISBN');
    
    
    $errorList = array();
     
    //sets a current date var
    $currentDate = date('Y-m-d', strtotime("now"));
    
    //queries first row where member email equals email
    $member = DB::queryFirstRow('SELECT * FROM members WHERE email=%s', $email);
    
    //validation handliing
    if(!$member)
    {
        array_push($errorList, "Membership doesnt exist");
        
    }
    
    if($member['expiryDate']<$currentDate)
    {
        array_push($errorList, "Membership has expired");
    }

    $assesDue = DB::queryFirstRow("SELECT * FROM loans WHERE memberId=%i AND ISBN=%s AND copyId=%s", $member['id'], $ISBN, $copyId);
    
    if(!$assesDue)
    {
        array_push($errorList, "No loan record found.");
    }
    else
    {
        $day = 0;
        $day += (strtotime($assesDue['dueDate']) - strtotime($currentDate)) / 60 / 60 / 24;
        if($day>=0)
        {
            $pay_due = 0;
        }
        else
        {
            $day=abs($day);
            $pay_due = $day * 0.1; 
        }  

    }
       
    //if errors this will display them
    if($errorList)
    {
        $app->render('admin/return_book.html.twig', array('errorList'=>$errorList, 'v'=>array('email'=>$email, 'copyId'=>$copyId, 'ISBN'=> $ISBN)));
    }
    else
    {
        //begin transaction
        DB::startTransaction();
        
        //insert into loan history 
        $insertLoanHist = DB::insert('loan_history', array('ISBN'=>$assesDue['ISBN'], 'outDate'=>$assesDue['outDate'], 'titleId'=>$assesDue['titleId'], 'memberId'=>$assesDue['memberId'], 'dueDate'=>$assesDue['dueDate'],
                'inDate'=>$currentDate, 'fineAssesed'=>$pay_due, 'finePaid'=>$pay_due));
        //if insert fails roll back
        if (!$insertLoanHist) 
        {    
            DB::rollback();
            $app->redirect('/internalerror');
        }
        // delete from loans 
        $deleteLoan = DB::delete('loans', 'memberId=%s AND ISBN=%s AND copyId=%s',$member['id'],$ISBN, $copyId);
        //if delete fails roll back
         if (!$deleteLoan) 
        {    
            DB::rollback();
            $app->redirect('/internalerror');
        }
        
        //update copies from onloan true to false
        $updateCopy = DB::update("copies", array('on_Loan'=>0),'id=%s',$copyId,'ISBN=%s',$ISBN,'titleId=%s',$assesDue['titleId']);
        
        //if fails roll back
        if (!$updateCopy) 
        {    
            DB::rollback();
            $app->redirect('/internalerror');
        }
        
        //update books where book id equals id, set loanable to true
        $updateBook = DB::update("books", array('loanable'=>1),'ISBN=%s',$ISBN,'titleId=%s',$assesDue['titleId']);
        
        //if false roll back
        if (!$updateBook) 
        {    
            DB::rollback();
            $app->redirect('/internalerror');
        }
        DB::commit();
        $app->render('admin/return_books_success.html.twig',array('email'=>$email,'copyId'=>$copyId, 'ISBN'=> $ISBN));
    }
    
});

//Re edit titles on all html.twig files
