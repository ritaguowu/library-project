-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jun 25, 2019 at 06:29 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `libraryaw`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `titleId` int(11) NOT NULL,
  `language` varchar(100) DEFAULT NULL,
  `category` enum('Kids','Cook','Westerns','Computer','Arts&Music') NOT NULL DEFAULT 'Computer',
  `cover` enum('hard','soft') DEFAULT NULL,
  `loanable` tinyint(1) NOT NULL DEFAULT '1',
  `imagePath` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `ISBN`, `titleId`, `language`, `category`, `cover`, `loanable`, `imagePath`) VALUES
(1, '1001', 1, 'English', 'Kids', 'hard', 1, '/img/book/book1.jfif'),
(2, '1002', 2, 'English', 'Kids', 'hard', 1, '/img/book/book2.jfif');

-- --------------------------------------------------------

--
-- Table structure for table `copies`
--

CREATE TABLE `copies` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `titleId` int(11) NOT NULL,
  `on_Loan` tinyint(1) NOT NULL DEFAULT '1',
  `renew_times` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `copies`
--

INSERT INTO `copies` (`id`, `ISBN`, `titleId`, `on_Loan`, `renew_times`) VALUES
(1, '1001', 1, 1, 1),
(2, '1001', 1, 0, 0),
(3, '1002', 2, 1, 1),
(4, '1002', 2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `copyId` int(11) NOT NULL,
  `titleId` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `outDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dueDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`id`, `ISBN`, `copyId`, `titleId`, `memberId`, `outDate`, `dueDate`) VALUES
(1, '1001', 1, 1, 4, '2019-06-24 18:11:43', '2019-06-18'),
(2, '1002', 3, 2, 4, '2019-06-02 18:12:13', '2019-06-11'),
(3, '1001', 2, 1, 3, '2019-06-24 19:12:50', '2019-07-10');

-- --------------------------------------------------------

--
-- Table structure for table `loan_history`
--

CREATE TABLE `loan_history` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `outDate` date NOT NULL,
  `titleId` int(11) NOT NULL,
  `MemberId` int(11) NOT NULL,
  `dueDate` date NOT NULL,
  `InDate` date NOT NULL,
  `fineAssesed` int(11) DEFAULT NULL,
  `finePaid` int(11) DEFAULT NULL,
  `remarks` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `expiryDate` date NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `role` enum('user','admin') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `lastName`, `firstName`, `expiryDate`, `address`, `email`, `role`) VALUES
(3, 'Guo', 'Wu', '2019-06-12', 'zXzxfsdfsdfsdffffffffffffffffffffvcvdsfsdfsdf', 'ritaguowu@gmail.com', 'admin'),
(4, 'T', 'Peter', '2020-05-20', 'rfsdsdfdsffsdfsdfsdfdsfsdfsfsf', 'aaa@bbb.ccc', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `ISBN` varchar(50) NOT NULL,
  `memberId` int(11) NOT NULL,
  `logDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `id` int(11) NOT NULL,
  `titleName` varchar(150) NOT NULL,
  `author` varchar(50) NOT NULL,
  `synopsis` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`id`, `titleName`, `author`, `synopsis`) VALUES
(1, 'A bumblebee sweater', 'Waterton, Betty, LaFave, Kim.', 'When Nellie tells her grandmother that she is going to play the part of a bumblebee in the spring concert, her grandmother knits her a bumblebee-striped sweater. Every time she wears the sweater, Nellie gets into a scrape.'),
(2, 'Dangerous!', 'Warnes, Tim.', 'Mole loves to stick labels on objects. One day, he finds something unusual in the woods. He puts a bunch of labels on the beast—lumpy, bumpy, gigantic. Then the thing frightens Mole, so Mole adds the label “dangerous.” But is the Lumpy-Bumpy Thing really all that dangerous?');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ISBN` (`ISBN`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `copies`
--
ALTER TABLE `copies`
  ADD PRIMARY KEY (`id`,`ISBN`),
  ADD KEY `ISBN` (`ISBN`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ISBN` (`ISBN`,`copyId`),
  ADD KEY `copyId` (`copyId`),
  ADD KEY `memberId` (`memberId`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `loan_history`
--
ALTER TABLE `loan_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ISBN` (`ISBN`),
  ADD KEY `MemberId` (`MemberId`),
  ADD KEY `titleId` (`titleId`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `memberId` (`memberId`),
  ADD KEY `ISBN` (`ISBN`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `copies`
--
ALTER TABLE `copies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `loan_history`
--
ALTER TABLE `loan_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`),
  ADD CONSTRAINT `books_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `copies` (`ISBN`);

--
-- Constraints for table `copies`
--
ALTER TABLE `copies`
  ADD CONSTRAINT `copies_ibfk_1` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`);

--
-- Constraints for table `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_ibfk_1` FOREIGN KEY (`copyId`) REFERENCES `copies` (`id`),
  ADD CONSTRAINT `loans_ibfk_2` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `loans_ibfk_3` FOREIGN KEY (`ISBN`) REFERENCES `copies` (`ISBN`),
  ADD CONSTRAINT `loans_ibfk_4` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`),
  ADD CONSTRAINT `loans_ibfk_5` FOREIGN KEY (`titleId`) REFERENCES `books` (`titleId`);

--
-- Constraints for table `loan_history`
--
ALTER TABLE `loan_history`
  ADD CONSTRAINT `loan_history_ibfk_1` FOREIGN KEY (`MemberId`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `loan_history_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `books` (`ISBN`),
  ADD CONSTRAINT `loan_history_ibfk_3` FOREIGN KEY (`titleId`) REFERENCES `titles` (`id`);

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `reservations_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `books` (`ISBN`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
