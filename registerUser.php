<?php

if (false) {
    $app = new \Slim\Slim();
}

$app->get('/registerUser/', function() use ($app) {

    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    }
    $user = $_SESSION['user'];
//$user = DB::queryFirstRow("SELECT * FROM members WHERE email='aaa@bbb.ccc'");

    if ($user) {
        $_SESSION['user'] = $user;
        $id = $_SESSION['user']['id'];

//Prepare the data for the register_user_master.html.twig
//1. Number of loans
        DB::query("SELECT * FROM loans WHERE memberId=%i ", $id);
        $loan_numb = DB::count();

//2.Items overdue
        $currentDate = date('Y-m-d', strtotime("now"));

        $overdue_books = DB::query("SELECT * FROM loans WHERE memberId=%i AND dueDate < %t", $id, $currentDate);
        $overdue_numb = DB::count();

//3.Items soon overdue
        $soon_overdue = date('Y-m-d', strtotime($currentDate . ' +3 days'));
//echo $soon_overdue;
        DB::query("SELECT * FROM loans WHERE memberId=%i AND dueDate <= %t AND dueDate >= %t", $id, $soon_overdue, $currentDate);
        $soondue_numb = DB::count();

//4.Payments due
        $day = 0;
        if ($overdue_books) {
//print_r($overdue_books);
            foreach ($overdue_books as $value) {
//calculate the difference between two dates (day)
                $day += abs(strtotime($value['dueDate']) - strtotime($currentDate)) / 60 / 60 / 24;
            }
        }
        $pay_due = $day * 0.1;
//echo $day;
//Number of reservations
        $currentDate = date('Y-m-d', strtotime("now"));

        $reserv_times = DB::queryFirstField("SELECT count(*) FROM reservations as r INNER JOIN books as b ON b.ISBN=r.ISBN INNER JOIN titles as t ON"
                        . " b.titleId=t.id  WHERE r.memberId=%i AND r.expiryDate>%t ORDER BY r.logDate", $id, $currentDate);

        $count = array('loan_numb' => $loan_numb, 'overdue_numb' => $overdue_numb, 'soondue_numb' => $soondue_numb, 'pay_due' => $pay_due, 'reserv_times' => $reserv_times);
        $app->render('/register_user/register_user_master.html.twig', array('v' => $user, 'count' => $count));
    } else {
        $app->redirect('/login');
        return;
    }
});


$app->get('/registerUser/:option', function($option) use ($app) {
    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    }
    $id = $_SESSION['user']['id'];
    $bookPerPage = 4;
    if ($option == "welcome") {

        $member = DB::queryFirstRow("SELECT * FROM members WHERE id=%i", $id);
        $app->render('/register_user/welcom.html.twig', array('v' => $member));
    } else
    if ($option == "loanRenew") {

//echo $id;
        $loans = DB::query("SELECT l.id, l.ISBN, t.titleName,b.category, b.imagePath, l.outDate,"
                        . " l.dueDate, c.renew_times FROM loans as l INNER JOIN titles as t ON"
                        . " l.titleId=t.id INNER JOIN books AS b ON l.titleId = b.titleId INNER JOIN copies as c "
                        . "ON l.copyId =c.id  WHERE l.memberId=%i order by l.dueDate", $id);

        DB::query("SELECT * FROM loans WHERE memberId=%i ", $id);
        $loan_times = DB::count();

//pagination-1: calculate total page numbers

        $totalPages = max(1, floor(($bookPerPage + $loan_times - 1 ) / $bookPerPage));

        $count = array('loan_times' => $loan_times, 'totalPages' => $totalPages);

        $app->render('/register_user/loadRenew.html.twig', array('loans' => $loans, 'count' => $count));
    } else
    if ($option == "loanHistory") {
        $loans = DB::query("SELECT lh.ISBN, t.titleName,b.category, b.imagePath, lh.outDate,"
                        . " lh.dueDate FROM loan_history as lh INNER JOIN titles as t ON"
                        . " lh.titleId=t.id INNER JOIN books AS b ON lh.titleId = b.titleId "
                        . "  WHERE lh.memberId=%i order by lh.dueDate desc", $id);
        DB::query("SELECT * FROM loan_history WHERE memberId=%i ", $id);
        $loan_times = DB::count();

        DB::query("SELECT * FROM reservations WHERE memberId=%i", $id);
        $reserv_times = DB::count();

        $totalPages = max(1, floor(($bookPerPage + $loan_times - 1 ) / $bookPerPage));

        $count = array('reserv_times' => $reserv_times, 'loan_times' => $loan_times, 'totalPages' => $totalPages);

        $app->render('/register_user/loadHistory.html.twig', array('loans' => $loans, 'count' => $count));
    }
    if ($option == "reserve") {
// echo "This is reservations page";
        $currentDate = date('Y-m-d', strtotime("now"));


        $reserv_times = DB::queryFirstField("SELECT count(*) FROM reservations as r INNER JOIN books as b ON b.ISBN=r.ISBN INNER JOIN titles as t ON"
                        . " b.titleId=t.id  WHERE r.memberId=%i AND r.expiryDate>%t ORDER BY r.logDate", $id, $currentDate);

//pagination-1: calculate total page numbers
        $totalPages = max(1, floor(($bookPerPage + $reserv_times - 1 ) / $bookPerPage));

        $count = array('reserv_times' => $reserv_times, 'totalPages' => $totalPages);

//print_r($count);
        $app->render('/register_user/reservations.html.twig', array('count' => $count));
    }
})->conditions(array('option' => '(welcome|loanRenew|loanHistory|reserve)'));

//pagination-1
$app->get('/ajax/books/:category/page/:page(/sortby/:order)', function($category, $page, $order) use ($app, $log) {
    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    }
    $id = $_SESSION['user']['id'];

    if ($order != null) {
        switch ($order) {
            case "btTitleAsc":
                $orderName = "t.titleName";
                break;
            case "btTitleDesc":
                $orderName = "t.titleName desc";
                break;
            case "btDueDateAsc":
                if ($category == "loadRenew")
                    $orderName = "l.dueDate";
                else
                    $orderName = "lh.dueDate";
                break;
            case "btDueDateDesc":
                if ($category == "loadRenew")
                    $orderName = "l.dueDate desc";
                else
                    $orderName = "lh.dueDate desc";
                break;
            default:
                $app->redirect('/internalerror');
                break;
        }
    }
//echo $category;
    $bookPerPage = 4;
    if ($category == "loadHistory") {

        $loansCount = DB::queryFirstField("SELECT COUNT(*) FROM loan_history as lh INNER JOIN titles as t ON"
                        . " lh.titleId=t.id INNER JOIN books AS b ON lh.titleId = b.titleId "
                        . "  WHERE lh.memberId=%i order by %l", $id, $orderName);
        if ($page * $bookPerPage > ($loansCount + $bookPerPage - 1)) { // TODO: make sure it's right
            $app->render('/register_user/no_record.html.twig');
            return;
        }
        $skip = ($page - 1 ) * $bookPerPage;
        $itemsList = DB::query("SELECT lh.ISBN, t.titleName,b.category, b.imagePath, lh.outDate,"
                        . " lh.dueDate FROM loan_history as lh INNER JOIN titles as t ON"
                        . " lh.titleId=t.id INNER JOIN books AS b ON lh.titleId = b.titleId "
                        . "  WHERE lh.memberId=%i ORDER BY %l LIMIT %l,%l", $id, $orderName, $skip, $bookPerPage);
    } else {
//$category == "loadRenew";
        $loansCount = DB::queryFirstField("SELECT COUNT(*) FROM loans as l INNER JOIN titles as t ON"
                        . " l.titleId=t.id INNER JOIN books AS b ON l.titleId = b.titleId INNER JOIN copies as c "
                        . "ON l.copyId =c.id  WHERE l.memberId=%i order by %l", $id, $orderName);
        if ($page * $bookPerPage > ($loansCount + $bookPerPage - 1)) { // TODO: make sure it's right
            $app->render('/register_user/no_record.html.twig');
            return;
        }
        $skip = ($page - 1 ) * $bookPerPage;
        $itemsList = DB::query("SELECT l.id, l.ISBN, t.titleName,b.category, b.imagePath, l.outDate,"
                        . " l.dueDate, c.renew_times FROM loans as l INNER JOIN titles as t ON"
                        . " l.titleId=t.id INNER JOIN books AS b ON l.titleId = b.titleId JOIN copies as c "
                        . "ON l.copyId =c.id  WHERE l.memberId=%i order by %l LIMIT %l,%l", $id, $orderName, $skip, $bookPerPage);
    }

    $app->render('/register_user/book_pages.html.twig', array('itemsList' => $itemsList, 'category' => $category));
//print_r($itemsList);
// $log->d('items: ' . print_r($itemsList, true));    
})->conditions(array('page' => '[1-9][0-9]*', 'category' => '(loadRenew|loadHistory)', 'order' => '(btTitleAsc|btTitleDesc|btDueDateAsc|btDueDateDesc)'));


$app->post('/registerUser/renewBook', function() use ($app) {
    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    }
    $id = $_SESSION['user']['id'];

    $value = $app->request->post('button');
    if ($value == 'printBook' || $value == 'sendEmail')
        return;

    $checkedItems = array();
    if ($value == "selectedBook") {
        if (isset($_POST['book'])) {
            //$checkedIds = $app->request->post('book');
            if (!empty($_POST['book'])) {
                // Counting number of checked checkboxes.
                $checked_count = count($_POST['book']);
                //echo "You have selected following " . $checked_count . " option(s): <br/>";
                foreach ($_POST['book'] as $item) {
                    $items = DB::queryFirstRow("SELECT copyId, ISBN FROM loans WHERE id = %i", $item);
                    array_push($checkedItems, $items);
                }
            } else {
                "Please at least choose one book.";
                return;
            }
        }
    } else if ($value == "allBook") {
        $checkedItems = DB::query("SELECT copyId, ISBN FROM loans WHERE memberId = %i", $id);
    }
//print_r($checkedItems);
//Update data to DB:
    DB::startTransaction();

    $renewBookName = array();
    $failedBookName = array();

    try{
    foreach ($checkedItems as $one) {
         //1. Update the renew_times in the table copies;
        $renewTimes = DB::queryOneField('renew_times', "SELECT * FROM copies WHERE id = %i and ISBN= %s", $one['copyId'], $one['ISBN']);

        //echo($renewTimes. "times");
        if ($renewTimes >= 2) {
            $result = DB::queryFirstRow("SELECT t.titleName FROM copies AS c INNER JOIN titles AS t ON c.titleId = t.id WHERE c.id=%i", $one['copyId']);
            array_push($failedBookName, $result['titleName']);
        } else {
        //echo "COPYID:" . $item['copyId'] . " ISBN:" . $item['ISBN'];
            $result = DB::update("copies", array('renew_times' => $renewTimes + 1), "id=%i and ISBN= %s", $one['copyId'], $one['ISBN']);
        //echo $result;
            if (!$result) {
                DB::rollback();
                $app->redirect('/internalerror');
            } else {
             //array_push($renewableBooks, $one);
            //print_r($renewableBooks);
            //2. Postpone the dueDate in the table loans.

                date_default_timezone_set('America/New_York');
                $currentDate = new DateTime();
                $currentDate = $currentDate->format('Y-m-d');

                $currentDate = date('Y-m-d', strtotime($currentDate . ' + 15 days'));
                //echo $currentDate; 
                $result = DB::update("loans", array('dueDate' => $currentDate), "copyId=%i and ISBN= %s", $one['copyId'], $one['ISBN']);
                if ($result) {

                    $result = DB::queryFirstRow("SELECT t.titleName FROM copies AS c INNER JOIN titles AS t ON c.titleId = t.id WHERE c.id=%i", $one['copyId']);
                //print_r($result);
                    array_push($renewBookName, $result['titleName']);
                } else {
                    DB::rollback();
                    $app->redirect('/internalerror');
                }
            }
        }
    }
    }catch (Exception $e){
        DB::rollback();
    }
// print_r($renewBookName);
    $app->render('register_user/renew_result.html.twig', array('v' => $renewBookName, 'f' => $failedBookName));
//echo "Renew all items successfully";
    DB::commit();
});


$app->get('/user/book-detail/:isbn', function($isbn) use ($app) {

    if (!$isbn) {
        $app->redirect('/internalerror');
    } else {

        $book = DB::queryFirstRow("SELECT * FROM books JOIN titles ON books.titleId = titles.id WHERE books.ISBN = %s", $isbn);
        $copies_numb = DB::queryFirstField("SELECT count(*) FROM copies WHERE ISBN=%s", $isbn);
        $reserv_number = DB::queryFirstField("SELECT count(*) FROM reservations WHERE ISBN=%s", $isbn);
        $skip = $reserv_number;
        if ($copies_numb == $reserv_number) {
//all the books have been reserved
            $availablity_date = null;
        } else {
            $availablity = DB::queryFirstRow("SELECT * FROM loans WHERE ISBN=%s order by dueDate LIMIT %l,%l", $isbn, $skip, 1);
            if (!$availablity) {
                $availablity_date = (new DateTime())->format('Y-m-d');  //nobody borrows this book which is available on current date.
            } else {
                $availablity_date = $availablity['dueDate'];
            }
        }

//echo $availablity_date;

        $info = array('copies_numb' => $copies_numb, 'reserv_number' => $reserv_number, 'availability' => $availablity_date);
        if ($book)
            $app->render('/register_user/book_detail.html.twig', array('book' => $book, 'info' => $info));
        else
            $app->redirect('/internalerror');
    }
});

$app->get('/user/search_backup', function() use ($app) {
    $search1 = $app->request->get('search1');
    $option1 = $app->request->get('option1');

    $operator1 = $app->request->get('operator1');
    $search2 = $app->request->get('search2');
    $option2 = $app->request->get('option2');

    $operator2 = $app->request->get('operator2');
    $search3 = $app->request->get('search3');
    $option3 = $app->request->get('option3');



//orgnize the where clause
    $operator1 = $search1 == "" ? "" : $operator1;
    $option1 = $search1 == "" ? "" : $option1;
    $operator1 = $search2 == "" ? "" : $operator1;
    $option2 = $search2 == "" ? "" : $option2;
    $operator2 = $search3 == "" ? "" : $operator2;
    $option3 = $search3 == "" ? "" : $option3;

    echo $search1 . " " . $option1 . " " . $operator1 . " " . $search2 . " " . $option2 . " " . $operator2 . " " . $search3 . " " . $option3;

    if ($search1 == "" && $search2 == "" && $search3 == "")
        $itemsList = DB::query("SELECT * FROM books INNER JOIN titles ON books.titleId = titles.id order by titles.titleName ");
    if ($itemsList)
        $app->render('/register_user/search_result.html.twig', array('itemsList' => $itemsList));
    else
        $app->redirect('/internalerror');
});


$app->get('/user/searchAll', function() use ($app) {
    $word = $app->request->get('searchText');
    $page = 1;
    $bookPerPage = 5;
    $resultCount = DB::queryFirstField("SELECT count(*) FROM books INNER JOIN titles ON books.titleId = titles.id WHERE titles.titleName Like %ss "
                    . "OR titles.author Like %ss OR books.ISBN LIKE %ss", $word, $word, $word);

    if ($page * $bookPerPage > ($resultCount + $bookPerPage - 1)) { // TODO: make sure it's right
        $app->render('/register_user/no_result.html.twig');
        return;
    }
    $skip = ($page - 1 ) * $bookPerPage;
    $itemsList = DB::query("SELECT * FROM books INNER JOIN titles ON books.titleId = titles.id WHERE titles.titleName Like %ss "
                    . "OR titles.author Like %ss OR books.ISBN LIKE %ss order by titles.titleName  LIMIT %l,%l", $word, $word, $word, $skip, $bookPerPage);

    $totalPages = max(1, floor(($bookPerPage + $resultCount - 1 ) / $bookPerPage));
//echo $totalPages;
    $count = array('resultCount' => $resultCount, 'totalPages' => $totalPages);
    if ($itemsList)
        $app->render('/register_user/searchAll_result.html.twig', array('itemsList' => $itemsList, 'count' => $count));
    else
        $app->render('/register_user/no_result.html.twig');
});


$app->get('/user/search/page', function() use ($app) {

    $filter = $app->request->get('filter');
//print_r($filter);
    $character = json_decode($filter);

    $search1 = $character->search1;
    $option1 = $character->option1;

    $operator1 = $character->operator1;
    $search2 = $character->search2;
    $option2 = $character->option2;

    $operator2 = $character->operator2;
    $search3 = $character->search3;
    $option3 = $character->option3;

    $page = $character->pageNum;
    $order = $character->order;

    if ($order != null) {
        switch ($order) {
            case "btTitleAsc":
                $orderName = "titles.titleName ASC";
                break;
            case "btTitleDesc":
                $orderName = "titles.titleName desc";
                break;
        }
    }
//orgnize the where clause
//echo  $option1 ." LIKE " . $search1 . " " . $operator1 . " " .$option2  . " LIKE " . $search2 . " " . $operator2 . " " . $option3 . " LIKE " . $search3;
//echo $pageNum ."". $orderName;
    $bookPerPage = 5;

    if ($operator1 == 'and' && $operator1 == 'and') {
        $where = new WhereClause("and");
        $where->add('1=%i', 1);

        if ($search1 != "") {
            $subclause = $where->addClause('and'); // create a WHERE statement of pieces joined by ANDs
            $subclause->add($option1 . ' LIKE %ss ', $search1);
        }

        if ($search2 != "") {
            $subclause = $where->addClause($operator1); // create a WHERE statement of pieces joined by operator1
            $subclause->add($option2 . ' LIKE %ss ', $search2);
        }

        if ($search3 != "") {
            $subclause = $where->addClause($operator2); // create a WHERE statement of pieces joined by operator1
            $subclause->add($option3 . ' LIKE %ss ', $search3);
        }
    } else if ($operator1 == 'or' && $operator1 == 'or') {
        $where = new WhereClause("or");

        if ($search1 != "") {
            $subclause = $where->addClause('or');
            $subclause->add($option1 . ' LIKE %ss ', $search1);
        }

        if ($search2 != "") {
            $subclause = $where->addClause($operator1); // create a WHERE statement of pieces joined by operator1
            $subclause->add($option2 . ' LIKE %ss ', $search2);
        }

        if ($search3 != "") {
            $subclause = $where->addClause($operator2); // create a WHERE statement of pieces joined by operator1
            $subclause->add($option3 . ' LIKE %ss ', $search3);
        }
    }

//print_r($where); have a problem of operator.

    $resultCount = DB::queryFirstField("SELECT count(*) FROM books INNER JOIN titles ON books.titleId = titles.id WHERE %l ORDER BY %l ", $where, $orderName);

    if ($page * $bookPerPage > ($resultCount + $bookPerPage - 1)) { // TODO: make sure it's right
        $app->render('/register_user/no_result.html.twig');
        return;
    }
    $skip = ($page - 1 ) * $bookPerPage;
    $itemsList = DB::query("SELECT * FROM books INNER JOIN titles ON books.titleId = titles.id WHERE %l order by %l  LIMIT %l,%l", $where, $orderName, $skip, $bookPerPage);

    $totalPages = max(1, floor(($bookPerPage + $resultCount - 1 ) / $bookPerPage));

    $count = array('resultCount' => $resultCount, 'totalPages' => $totalPages);
    if ($itemsList)
        $app->render('/register_user/search_result_page.html.twig', array('itemsList' => $itemsList, 'count' => $count));
    else
        $app->render('/register_user/no_result.html.twig');
});


$app->get('/internalerror', function() use ($app, $log) {
    $app->render("internal_error.html.twig");
});

$app->get('/reservations/btreserveClick', function() use ($app, $log) {
    $isbn = $app->request->get('isbn');


    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    } else {
        $id = $_SESSION['user']['id'];

        if (!$isbn) {
            $app->redirect('/internalerror');
        } else {
            $currentDate = date('Y-m-d', strtotime("now"));
            $expiryDate = date('Y-m-d', strtotime($currentDate . ' +30 days'));
            $totalCopies = DB::queryFirstField("SELECT count(*) FROM copies WHERE ISBN=%s", $isbn);
            $totalReserve = DB::queryFirstField("SELECT count(*) FROM reservations WHERE ISBN=%s AND expiryDate > %s", $isbn, $currentDate);
//echo $totalReserve +"\n";
            if ($totalCopies > $totalReserve) {
                $result = DB::insert('reservations', array('ISBN' => $isbn, 'memberId' => $id, 'expiryDate' => $expiryDate));
                if (!$result)
                    $app->redirect('/internalerror');
                else {
                    echo "You have already reserved successfully!\n We will reserve it for you till " . $expiryDate . "\n";
                }
            } else {
                echo "Sorry. This book is not available to reserve. Please check it's Avaliablity Date.";
            }
        }
    }
});

//$app->get('/registerUser/btreserve', function() use ($app, $log) {
//     if (!isset($_SESSION['user'])) {
//        $app->redirect('/login');
//        return;
//    }
//    $id = $_SESSION['user']['id'];
//    
//    $currentDate = date('Y-m-d', strtotime("now"));
//    $itemsList = DB::query("SELECT * FROM reservations WHERE memberId=%i AND expiryDate<%s", $id,$currentDate );
//     $app->render('/register_user/reservations.html.twig', array('itemsList' => $itemsList));
//});



$app->get('/ajax/books/reserve/page/:page(/sortby/:order)', function($page, $order) use ($app, $log) {
//echo "This is reserve page details";
    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    }
    $id = $_SESSION['user']['id'];

    if ($order != null) {
        switch ($order) {
            case "btTitleAsc":
                $orderName = "t.titleName";
                break;
            case "btTitleDesc":
                $orderName = "t.titleName desc";
                break;
            case "btReserveAsc":
                $orderName = "r.logDate";
                break;
            case "btReserveDesc":
                $orderName = "r.logDate desc";
                break;
            default:
                $app->redirect('/internalerror');
                break;
        }
        $_SESSION['order'] = $order;
    }

    $bookPerPage = 4;
    $currentDate = date('Y-m-d', strtotime("now"));
    $reservCount = DB::queryFirstField("SELECT COUNT(*) FROM reservations as r INNER JOIN books as b ON b.ISBN=r.ISBN INNER JOIN titles as t ON"
                    . " b.titleId=t.id  WHERE r.memberId=%i AND expiryDate>%t", $id, $currentDate);
    if ($page * $bookPerPage > ($reservCount + $bookPerPage - 1)) { // TODO: make sure it's right
        $app->render('/register_user/no_record.html.twig');
        return;
    }
    $skip = ($page - 1 ) * $bookPerPage;
    $itemsList = DB::query("SELECT r.id, r.ISBN, t.titleName, b.imagePath, b.category, r.logDate,"
                    . " r.expiryDate FROM reservations as r INNER JOIN books as b ON b.ISBN=r.ISBN INNER JOIN titles as t ON"
                    . " b.titleId=t.id  WHERE r.memberId=%i AND expiryDate>%s ORDER BY %l LIMIT %l,%l", $id, $currentDate, $orderName, $skip, $bookPerPage);

    $app->render('/register_user/reserv_pages.html.twig', array('itemsList' => $itemsList));
//print_r($itemsList);
// $log->d('items: ' . print_r($itemsList, true));    
})->conditions(array('page' => '[1-9][0-9]*', 'order' => '(btTitleAsc|btTitleDesc|btReserveAsc|btReserveDesc)'));


$app->get('/reservations/reserveSelected', function() use ($app, $log) {

//print_r($isbnArray);
//print_r(count($isbnArray));
    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    }
    $id = $_SESSION['user']['id'];

    $currentDate = date('Y-m-d', strtotime("now"));
    $isbns = $app->request->get('reserv');
    $isbnArray = array();

    if (!$isbns) {
        echo "Please choose at least 1 reservation book.";
    } else {
        if ($isbns == 'all') {
            $isbns = DB::query("SELECT * FROM reservations WHERE memberId=%i AND expiryDate>%t", $id, $currentDate);
            foreach ($isbns as $v) {
                array_push($isbnArray, $v['ISBN']);
            }
        } else {
            $isbnArray = explode(",", $isbns);
//print_r($isbnArray);
        }
//1.-- Make sure the member is available to borrow a book
        $validMember = DB::queryFirstRow("SELECT * FROM members WHERE id = %i AND expiryDate > %t", $id, $currentDate);
        if (!$validMember) {
            echo "You member card is expired. Please bring your valid ID to apply again.";
            return;
        }

//2. -- Only 25 books can be loaned for each member
//already loaned books
        $loaned_numb = DB::queryFirstField("SELECT count(*) FROM loans WHERE memberId = %s", $id);
//echo $loaned_numb;
        if ($loaned_numb + count($isbnArray) > 25) {
            echo "The total number of loan books should be less than 25 books.";
        }

        DB::startTransaction();
        try{
        foreach ($isbnArray as $value) {
            echo $value;
//3.-- Only loanable book can be loaned by members
            $loanable = DB::queryFirstField("SELECT loanable FROM books WHERE ISBN = %s", $value);

//echo $loanable. "\n";
            if ($loanable) {
//4. -- Insert a loan record into loan table with due_Date 14 days after the current date
                $firstAvailableCopy = DB::queryFirstRow("SELECT * from copies as c INNER JOIN books as b ON b.ISBN = c.ISBN WHERE c.isbn = %s AND c.on_Loan = 0", $value);
//print_r($firstAvailableCopy);
//5.-- check if there is the same book in loans table. If yes, return error.
                $sameBook = DB::queryFirstRow("SELECT * FROM loans WHERE ISBN=%s AND copyId=%s", $firstAvailableCopy['ISBN'], $firstAvailableCopy['id']);
                if (!$sameBook) {
                    $insertLoan = DB::insert('loans', array('ISBN' => $firstAvailableCopy['ISBN'], 'copyId' => $firstAvailableCopy['id'], 'titleId' => $firstAvailableCopy['titleId'],
                                'memberId' => $id, 'outDate' => $currentDate, 'dueDate' => date('Y-m-d', strtotime($currentDate . ' +14 days'))));
                    if (!$insertLoan) {
                        DB::rollback();
                        $app->redirect('/internalerror');
                    }
//6. -- Change the state of this copy book (on_loan) in copies table to 1 
                    DB::update('copies', array('on_Loan' => 1), "ISBN = %s AND id = %s", $firstAvailableCopy['ISBN'], $firstAvailableCopy['id']);
//7. -- check if all the books with same ISBN are on-loan in copy table 
                    $totalBooks = DB::queryFirstField("SELECT count(*) FROM copies WHERE ISBN = %s", $firstAvailableCopy['ISBN']);
                    $onloanBooks = DB::queryFirstField("SELECT count(*) FROM copies WHERE ISBN = %s AND on_loan = 1", $firstAvailableCopy['ISBN']);
//echo $totalBooks;
//echo $onloanBooks;
                    if ($totalBooks <= $onloanBooks) {
//8.--Update the state in books (loanable) to 0 , which means all the copied is on loaned.
                        DB::update('books', array('loanable' => 0), "ISBN = %s", $firstAvailableCopy['ISBN']);
//9. --Delete the record from reservations table
                        DB::delete("reservations", "memberId=%i AND ISBN=%s", $id, $firstAvailableCopy['ISBN']);
                    }
                    echo " loan books successful\n";
                } else {
                    echo " This book has already loaned\n";
                }
            } else {
                echo " This book can not loanable\n";
            }
        }
        } catch (Exception $e){
            DB:: rollback();
        }
        DB::commit();
    }
});


$app->get('/reservations/reserveDelete', function() use ($app, $log) {

//print_r($isbnArray);
//print_r(count($isbnArray));
    if (!isset($_SESSION['user'])) {
        $app->redirect('/login');
        return;
    }
    $id = $_SESSION['user']['id'];

    if (isset($_SESSION['order'])) {
        $order = $_SESSION['order'];
    }
    $reservId = $app->request->get('reservId');
    DB::delete("reservations", "id=%i", $reservId);
    echo $order;
});

$app->get('/registerUser/payment', function() use ($app, $log) {
    if (!isset($_SESSION['user'])) {
        //echo "dsfdsf";
        $app->redirect('/login');
        return;
    }
    $id = $_SESSION['user']['id'];
    echo $id;
    $result = DB::query("SELECT * FROM loan_history WHERE memberId=%i", $id);
//    if ($result==null){
//        $app->render('/register_user/no_record.html.twig');
//    }
    $totalFineAssesed = DB::queryFirstField("SELECT sum(fineAssesed) FROM loan_history WHERE memberId=%i", $id);
    $totalFinePaid = DB::queryFirstField("SELECT sum(finePaid) FROM loan_history WHERE memberId=%i", $id);

    $fineRecords = DB::query("SELECT * FROM loan_history JOIN titles ON loan_history.titleId = titles.id WHERE memberId=%i AND (fineAssesed IS NOT NULL OR finePaid IS NOT NULL)", $id);

// print_r($totalFineAssesed);
    $info = array('userId' => $id, 'totalFineAssesed' => $totalFineAssesed, 'totalFinePaid' => $totalFinePaid);
    $app->render('/register_user/paymentHistory.html.twig', array('fineRecords' => $fineRecords, 'info' => $info));
});
