<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* internal_error.html.twig */
class __TwigTemplate_24a94e94d01261b509d9d13c66403649e18babe45eaa0902a16882be1fd0fc42 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "internal_error.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        // line 5
        echo "Problem
";
    }

    // line 7
    public function block_headAdd($context, array $blocks = [])
    {
        // line 8
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/stylesAdd.css\">
";
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        // line 11
        echo "    <div id=\"centerContentInternalError\">
    <h3>Internal error</h3>
    <img src=\"/img/codingcat.gif\" width='200' alt=\"error\">
    <P> Internal error. We are working on it. We apologize for the inconvenience. <a href=\"/\"> click to continue </a></p>
    </div>
";
    }

    public function getTemplateName()
    {
        return "internal_error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 11,  57 => 10,  52 => 8,  49 => 7,  44 => 5,  41 => 4,  31 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# empty Twig template #}
{% extends \"master.html.twig\" %}

{% block title %}
Problem
{% endblock %}
{% block headAdd  %}
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/stylesAdd.css\">
{% endblock headAdd %}
{% block content %}
    <div id=\"centerContentInternalError\">
    <h3>Internal error</h3>
    <img src=\"/img/codingcat.gif\" width='200' alt=\"error\">
    <P> Internal error. We are working on it. We apologize for the inconvenience. <a href=\"/\"> click to continue </a></p>
    </div>
{% endblock %}", "internal_error.html.twig", "C:\\xampp\\htdocs\\ipd17\\library-project\\templates\\internal_error.html.twig");
    }
}
