<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* homepage.html.twig */
class __TwigTemplate_a480ce4db043f118f2e3e7594dcbcfd651120db01233de9f5e83f4071e55545d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "homepage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        // line 4
        echo "    LibraryAW Homepage
";
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        // line 7
        echo "  
    <!-- Photo by Sylvia Yang on Unsplash -->
    <img src=\"img\\homepage5.jpg\" alt=\"Library photo\"> </img>
    <hr>
    <div class=\"row\">
            <div class=\"col-4 p-5 mb-4 bg-dark text-white text-left\">
                <p class=\"h4 text-center\">Opening Hours</p>
                <span class=\"border-top my-3\"><hr></span>
                <div class=\"col-12 h6 \">
                    <div class=\"row p-2 text-warning\">
                        <div class=\"col-6 \">
                            Sunday
                        </div>
                        <div class=\"col-6\">
                            10:00-17:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Monday
                        </div>
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Tuesday
                        </div>                
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>  
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Wednesday
                        </div>                
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Thurday
                        </div>
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Friday
                        </div>                
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>   
                    </div>
                    <div class=\"row p-2 text-warning\">
                        <div class=\"col-6\">
                            Saturday
                        </div>                
                        <div class=\"col-6\">
                            10:00-17:00
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"col-4 h6 text-left\">
                <p class=\"h5 text-left font-weight-bold\">UPCOMING EVENTS</p>
                <span class=\"border-top my-3\"><hr></span>
                <p class=\"bg-dark text-white\"> Friday June 28,2019</p>
                <p class=\"bg-light font-weight-bold \">Title1</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores</p>
                <p class=\"bg-dark text-white\"> Friday June 28,2019</p>
                <p class=\"bg-light font-weight-bold \">Title2</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores</p>

            </div>
            <div class=\"col-4 border\">
                <div class=\"col-12 text-center\">
                    <button type=\"button\" class=\"btn btn-large btn-dark btnTrans2\" >I need help</button>
                </div>
                <div>
                    <!-- Aaron, do you think your live chat can be implement here? -->
                </div>
            </div>
        </div>
    
";
    }

    public function getTemplateName()
    {
        return "homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 7,  48 => 6,  43 => 4,  40 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}
    LibraryAW Homepage
{% endblock %}
{% block content %}
  
    <!-- Photo by Sylvia Yang on Unsplash -->
    <img src=\"img\\homepage5.jpg\" alt=\"Library photo\"> </img>
    <hr>
    <div class=\"row\">
            <div class=\"col-4 p-5 mb-4 bg-dark text-white text-left\">
                <p class=\"h4 text-center\">Opening Hours</p>
                <span class=\"border-top my-3\"><hr></span>
                <div class=\"col-12 h6 \">
                    <div class=\"row p-2 text-warning\">
                        <div class=\"col-6 \">
                            Sunday
                        </div>
                        <div class=\"col-6\">
                            10:00-17:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Monday
                        </div>
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Tuesday
                        </div>                
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>  
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Wednesday
                        </div>                
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Thurday
                        </div>
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>
                    </div>
                    <div class=\"row p-2\">
                        <div class=\"col-6\">
                            Friday
                        </div>                
                        <div class=\"col-6\">
                            10:00-21:00
                        </div>   
                    </div>
                    <div class=\"row p-2 text-warning\">
                        <div class=\"col-6\">
                            Saturday
                        </div>                
                        <div class=\"col-6\">
                            10:00-17:00
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"col-4 h6 text-left\">
                <p class=\"h5 text-left font-weight-bold\">UPCOMING EVENTS</p>
                <span class=\"border-top my-3\"><hr></span>
                <p class=\"bg-dark text-white\"> Friday June 28,2019</p>
                <p class=\"bg-light font-weight-bold \">Title1</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores</p>
                <p class=\"bg-dark text-white\"> Friday June 28,2019</p>
                <p class=\"bg-light font-weight-bold \">Title2</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores</p>

            </div>
            <div class=\"col-4 border\">
                <div class=\"col-12 text-center\">
                    <button type=\"button\" class=\"btn btn-large btn-dark btnTrans2\" >I need help</button>
                </div>
                <div>
                    <!-- Aaron, do you think your live chat can be implement here? -->
                </div>
            </div>
        </div>
    
{% endblock %}
", "homepage.html.twig", "C:\\xampp\\htdocs\\ipd17\\library-project\\templates\\homepage.html.twig");
    }
}
