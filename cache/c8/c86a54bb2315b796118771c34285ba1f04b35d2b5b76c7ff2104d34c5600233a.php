<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /register_user/loadRenew.html.twig */
class __TwigTemplate_e7ae094a0e1807baae980a065001a3f6628e4b7e2f767746f4b2b46813251cb5 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "<script>
    function loadPage(pageNum, order='btDueDateAsc') {

        \$(\"#detail\").load('/ajax/books/loadRenew/page/' + pageNum +'/sortby/'+ order);
    }
    \$(document).ready(function () {
        loadPage(1,'btDueDateAsc');
        \$('.optionBtn').click(function () {
            var order = \$(this).attr('name');
            var orderby =\$(this).text();
            \$(\"#orderby\").html(orderby);
            loadPage(1, order);
        });
    });

</script>

    <h3 class=\"mt-4\">Loans and Renewals</h3>
    <hr>
    Renewal is effective from the current date.
    <p>
    <form method=\"post\" action=\"/registerUser/renewBook\" class=\"forPage\">
        <div>
            <button type=\"submit\" name=\"button\" value=\"selectedBook\" class=\"btn btn-primary btn-sm\">Renew selected loans</button>
            <button type=\"submit\"  name=\"button\" value=\"allBook\" class=\"btn btn-primary btn-sm\">Renew all loans</button>
            <button type=\"submit\"  name=\"button\" value=\"printBook\" class=\"btn btn-primary btn-sm\">Print</button>
            <button type=\"submit\"  name=\"button\" value=\"sendEmail\" class=\"btn btn-primary btn-sm\">Send by email</button>
        </div>
        <P></P>
        <span>Total ";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "loan_times", []), "html", null, true);
        echo " books</span><p>
        <div class='row'>
            <span class=\"font-weight-bold\">Sort by:</span>
            <div class=\"dropdown\">
                <button class=\"btn btn-primary dropdown-toggle\" type=\"button\" id=\"dropdownMenu2\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    <span id=\"orderby\">Due date (ascending)</span>
                </button>
                <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btTitleAsc\">Title (ascending)</button>
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btTitleDesc\">Title (descending)</button>
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btDueDateAsc\">Due date (ascending)</button>
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btDueDateDesc\">Due date (descending)</button>
                </div>
            </div>
        </div>
        <p>
        <p>
    <div id=\"detail\">
       </div>
        <p>
        <p>
         <div id=\"pageNavigation\" class=\"text-center\">
        ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "totalPages", [])));
        foreach ($context['_seq'] as $context["_key"] => $context["num"]) {
            // line 54
            echo "            <a href=\"#\" onClick=\"loadPage(";
            echo twig_escape_filter($this->env, $context["num"], "html", null, true);
            echo ")\">";
            echo twig_escape_filter($this->env, $context["num"], "html", null, true);
            echo "</a>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['num'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "     
   </form> ";
    }

    public function getTemplateName()
    {
        return "/register_user/loadRenew.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 55,  90 => 54,  86 => 53,  61 => 31,  30 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# empty Twig template #}
<script>
    function loadPage(pageNum, order='btDueDateAsc') {

        \$(\"#detail\").load('/ajax/books/loadRenew/page/' + pageNum +'/sortby/'+ order);
    }
    \$(document).ready(function () {
        loadPage(1,'btDueDateAsc');
        \$('.optionBtn').click(function () {
            var order = \$(this).attr('name');
            var orderby =\$(this).text();
            \$(\"#orderby\").html(orderby);
            loadPage(1, order);
        });
    });

</script>

    <h3 class=\"mt-4\">Loans and Renewals</h3>
    <hr>
    Renewal is effective from the current date.
    <p>
    <form method=\"post\" action=\"/registerUser/renewBook\" class=\"forPage\">
        <div>
            <button type=\"submit\" name=\"button\" value=\"selectedBook\" class=\"btn btn-primary btn-sm\">Renew selected loans</button>
            <button type=\"submit\"  name=\"button\" value=\"allBook\" class=\"btn btn-primary btn-sm\">Renew all loans</button>
            <button type=\"submit\"  name=\"button\" value=\"printBook\" class=\"btn btn-primary btn-sm\">Print</button>
            <button type=\"submit\"  name=\"button\" value=\"sendEmail\" class=\"btn btn-primary btn-sm\">Send by email</button>
        </div>
        <P></P>
        <span>Total {{count.loan_times}} books</span><p>
        <div class='row'>
            <span class=\"font-weight-bold\">Sort by:</span>
            <div class=\"dropdown\">
                <button class=\"btn btn-primary dropdown-toggle\" type=\"button\" id=\"dropdownMenu2\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    <span id=\"orderby\">Due date (ascending)</span>
                </button>
                <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btTitleAsc\">Title (ascending)</button>
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btTitleDesc\">Title (descending)</button>
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btDueDateAsc\">Due date (ascending)</button>
                    <button class=\"dropdown-item optionBtn\" type=\"button\" name=\"btDueDateDesc\">Due date (descending)</button>
                </div>
            </div>
        </div>
        <p>
        <p>
    <div id=\"detail\">
       </div>
        <p>
        <p>
         <div id=\"pageNavigation\" class=\"text-center\">
        {% for num in range(1, count.totalPages) %}
            <a href=\"#\" onClick=\"loadPage({{num}})\">{{num}}</a>
        {% endfor %}     
   </form> ", "/register_user/loadRenew.html.twig", "C:\\xampp\\htdocs\\ipd17\\library-project\\templates\\register_user\\loadRenew.html.twig");
    }
}
