<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b25f657bebba3ce9a1e09940df558670128dce631c67ab7394b7edabd4b6aced extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<html lang=\"en\">
    <head>

        <!-- Required meta tags -->
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

        <!--favicon.ico error -->
        <link rel=\"shortcut icon\" href=\"\">

        <!-- Bootstrap CSS -->
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">

        <!-- Font Awesome CSS-->
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">

        <!-- laod js -->
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\t

        <!-- load Ajax -->
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

        <!-- OUR CSS-->
        <link rel=\"stylesheet\" href=\"/css/style.css\">
        <title>";
        // line 27
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"description\" content=\"LibraryAW can provide choose books online, and pickup at libraryAW. User can reserve books when all the books are not loanable.\"/>
        <meta name=\"keywords\" content=\"libray, books, online, reservation, reserve, loan, payment\"/>

        <!-- Add specific head for each page -->
        ";
        // line 32
        $this->displayBlock('headAdd', $context, $blocks);
        // line 34
        echo "
    </head>
    <body>

        <header>
            <!-- logo area  -->
            <div class=\"media\">
                <div class=\"media-body\">
                    <div class=\"container-fluid header-area\">
                        <div class=\"row\">     
                            <div class=\"user-menu col-md-8\">
                                <ul class=\"row m-t-1 p-l-1\">
                                    <li><a href=\"/login\"><i class=\"fa fa-user\"></i> Login</a></li>
                                    <li><a href=\"/member/register\"><i class=\"fa fa-user\"></i> Register</a></li>
                                    <li><a href=\"/login\"><i class=\"fa fa-user\"></i> My Account</a></li>
                                    <li><a href=\"/logout\"><i class=\"fa fa-heart\"></i>Logout</a></li>

                                </ul>
                            </div>


                            <div class=\"header-right col-md-3\">
                                <ul class=\"list-unstyled list-inline\">
                                    <li class=\"dropdown\">
                                        <a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"key\">Currency :</span><span class=\"value\">CAD </span><b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu text-center\">
                                            <li><a href=\"#\">USD</a></li>
                                            <li><a href=\"#\">CAD</a></li>
                                        </ul>
                                    </li>

                                    <li class=\"dropdown language\">
                                        <a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"key\">Language :</span><span class=\"value\">English </span><b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu text-center\">
                                            <li><a href=\"#\">English</a></li>
                                            <li><a href=\"#\">French</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>



                    <!-- mainmenu area  -->

                    <div class=\"mainmenu-area\">

                        <nav class=\"navbar  navbar-expand-lg navbar-dark bg-dark\">
                            <a class=\"navbar-brand\" href=\"#\"></a>
                            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                                <span class=\"navbar-toggler-icon\"></span>
                            </button>

                            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                                <ul class=\"navbar-nav mr-auto\">
                                    <li class=\"nav-item active\">
                                        <a class=\"nav-link\" href=\"/\">HOME<span class=\"sr-only\">(current)</span></a>
                                    </li>
                                    <li class=\"nav-item\">
                                        <a class=\"nav-link\" href=\"#\">Services</a>
                                    </li>
                                    <li class=\"nav-item\">
                                        <a class=\"nav-link\" href=\"#\">Collection</a>
                                    </li>

                                    <li class=\"nav-item dropdown\">
                                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                            Programs&Events
                                        </a>
                                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                                            <a class=\"dropdown-item\" href=\"#\">All Events</a>
                                            <a class=\"dropdown-item\" href=\"#\">All Programs</a>
                                            <a class=\"dropdown-item\" href=\"#\">Children&Teen Programs</a>
                                        </div>
                                    </li>
                                </ul>
                                <form class=\"form-inline my-2 my-lg-0\" action=\"/user/searchAll\">
                                    <input class=\"form-control mr-sm-2\" name=\"searchText\" type=\"text\" placeholder=\"Book, DVD, author, etc...\" aria-label=\"Search\">
                                    <span><input type=\"submit\" class=\"btn btn-outline-success my-2 my-sm-0\" id =\"searchBtn\" value=\"Search\"></span>
                                </form>

                            </div>
                        </nav>
                    </div>
                </div>
            </div> 

        </header>
        <div class=\"container-fluid\" id=\"searchContent\">
            ";
        // line 125
        $this->displayBlock('content', $context, $blocks);
        // line 128
        echo "        </div>
        <!-- footer area -->
        <footer class=\"bottom\">
            <div class=\"bg-dark text-success\">

                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-4 col-sm-6\">
                            <div class=\"footer-about-us\">
                                <h3><span>About LibraryAW</span></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores </p>
                                <div class=\"footer-social\">
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-facebook-f\"></i></a>
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-twitter\"></i></a>
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-google\"></i></a>
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-linkedin-in\"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class=\"col-md-4 col-sm-6\">
                            <div class=\"footer-menu\">
                                <h3 class=\"footer-wid-title\">User Navigation </h3>
                                <ul>
                                    <li><a href=\"\">My account</a></li>
                                    <li><a href=\"\">My reservation</a></li>
                                    <li><a href=\"\">contact us</a></li>

                                </ul>
                            </div>
                        </div>

                        <div class=\"col-md-4 col-sm-6\">
                            <div class=\"footer-newsletter\">
                                <h3 class=\"footer-wid-title\">Newsletter</h3>
                                <p>Sign up to our newsletter and get exclusive deals straight to your inbox!</p>
                                <div class=\"newsletter-form\">
                                    <input type=\"email\" placeholder=\"Type your email\"> 
                                    <input type=\"submit\" value=\"Subscribe\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-8\">
                            <div class=\"copyright text-white text-center\">
                                <p>&copy; Copyright by JohnAbbott LibraryAW 2019.</p>
                            </div>
                        </div>

                        <div class=\"col-md-4\">
                            <div class=\"footer-card-icon\">
                                <i class=\"fab fa-cc-discover\"></i>
                                <i class=\"fab fa-cc-mastercard\"></i>
                                <i class=\"fab fa-cc-paypal\"></i>
                                <i class=\"fab fa-cc-visa\"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>

";
    }

    // line 27
    public function block_title($context, array $blocks = [])
    {
        echo " ";
    }

    // line 32
    public function block_headAdd($context, array $blocks = [])
    {
        // line 33
        echo "        ";
    }

    // line 125
    public function block_content($context, array $blocks = [])
    {
        // line 126
        echo "                <!-- Add specific head for each page -->
            ";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  251 => 126,  248 => 125,  244 => 33,  241 => 32,  235 => 27,  166 => 128,  164 => 125,  71 => 34,  69 => 32,  61 => 27,  33 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<html lang=\"en\">
    <head>

        <!-- Required meta tags -->
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

        <!--favicon.ico error -->
        <link rel=\"shortcut icon\" href=\"\">

        <!-- Bootstrap CSS -->
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">

        <!-- Font Awesome CSS-->
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">

        <!-- laod js -->
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\t

        <!-- load Ajax -->
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

        <!-- OUR CSS-->
        <link rel=\"stylesheet\" href=\"/css/style.css\">
        <title>{% block title %} {% endblock %}</title>
        <meta name=\"description\" content=\"LibraryAW can provide choose books online, and pickup at libraryAW. User can reserve books when all the books are not loanable.\"/>
        <meta name=\"keywords\" content=\"libray, books, online, reservation, reserve, loan, payment\"/>

        <!-- Add specific head for each page -->
        {%block headAdd %}
        {% endblock %}

    </head>
    <body>

        <header>
            <!-- logo area  -->
            <div class=\"media\">
                <div class=\"media-body\">
                    <div class=\"container-fluid header-area\">
                        <div class=\"row\">     
                            <div class=\"user-menu col-md-8\">
                                <ul class=\"row m-t-1 p-l-1\">
                                    <li><a href=\"/login\"><i class=\"fa fa-user\"></i> Login</a></li>
                                    <li><a href=\"/member/register\"><i class=\"fa fa-user\"></i> Register</a></li>
                                    <li><a href=\"/login\"><i class=\"fa fa-user\"></i> My Account</a></li>
                                    <li><a href=\"/logout\"><i class=\"fa fa-heart\"></i>Logout</a></li>

                                </ul>
                            </div>


                            <div class=\"header-right col-md-3\">
                                <ul class=\"list-unstyled list-inline\">
                                    <li class=\"dropdown\">
                                        <a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"key\">Currency :</span><span class=\"value\">CAD </span><b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu text-center\">
                                            <li><a href=\"#\">USD</a></li>
                                            <li><a href=\"#\">CAD</a></li>
                                        </ul>
                                    </li>

                                    <li class=\"dropdown language\">
                                        <a data-toggle=\"dropdown\" data-hover=\"dropdown\" class=\"dropdown-toggle\" href=\"#\"><span class=\"key\">Language :</span><span class=\"value\">English </span><b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu text-center\">
                                            <li><a href=\"#\">English</a></li>
                                            <li><a href=\"#\">French</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>



                    <!-- mainmenu area  -->

                    <div class=\"mainmenu-area\">

                        <nav class=\"navbar  navbar-expand-lg navbar-dark bg-dark\">
                            <a class=\"navbar-brand\" href=\"#\"></a>
                            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                                <span class=\"navbar-toggler-icon\"></span>
                            </button>

                            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                                <ul class=\"navbar-nav mr-auto\">
                                    <li class=\"nav-item active\">
                                        <a class=\"nav-link\" href=\"/\">HOME<span class=\"sr-only\">(current)</span></a>
                                    </li>
                                    <li class=\"nav-item\">
                                        <a class=\"nav-link\" href=\"#\">Services</a>
                                    </li>
                                    <li class=\"nav-item\">
                                        <a class=\"nav-link\" href=\"#\">Collection</a>
                                    </li>

                                    <li class=\"nav-item dropdown\">
                                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                            Programs&Events
                                        </a>
                                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                                            <a class=\"dropdown-item\" href=\"#\">All Events</a>
                                            <a class=\"dropdown-item\" href=\"#\">All Programs</a>
                                            <a class=\"dropdown-item\" href=\"#\">Children&Teen Programs</a>
                                        </div>
                                    </li>
                                </ul>
                                <form class=\"form-inline my-2 my-lg-0\" action=\"/user/searchAll\">
                                    <input class=\"form-control mr-sm-2\" name=\"searchText\" type=\"text\" placeholder=\"Book, DVD, author, etc...\" aria-label=\"Search\">
                                    <span><input type=\"submit\" class=\"btn btn-outline-success my-2 my-sm-0\" id =\"searchBtn\" value=\"Search\"></span>
                                </form>

                            </div>
                        </nav>
                    </div>
                </div>
            </div> 

        </header>
        <div class=\"container-fluid\" id=\"searchContent\">
            {% block content %}
                <!-- Add specific head for each page -->
            {% endblock %}
        </div>
        <!-- footer area -->
        <footer class=\"bottom\">
            <div class=\"bg-dark text-success\">

                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-4 col-sm-6\">
                            <div class=\"footer-about-us\">
                                <h3><span>About LibraryAW</span></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores </p>
                                <div class=\"footer-social\">
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-facebook-f\"></i></a>
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-twitter\"></i></a>
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-google\"></i></a>
                                    <a href=\"#\" target=\"_blank\"><i class=\"fab fa-linkedin-in\"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class=\"col-md-4 col-sm-6\">
                            <div class=\"footer-menu\">
                                <h3 class=\"footer-wid-title\">User Navigation </h3>
                                <ul>
                                    <li><a href=\"\">My account</a></li>
                                    <li><a href=\"\">My reservation</a></li>
                                    <li><a href=\"\">contact us</a></li>

                                </ul>
                            </div>
                        </div>

                        <div class=\"col-md-4 col-sm-6\">
                            <div class=\"footer-newsletter\">
                                <h3 class=\"footer-wid-title\">Newsletter</h3>
                                <p>Sign up to our newsletter and get exclusive deals straight to your inbox!</p>
                                <div class=\"newsletter-form\">
                                    <input type=\"email\" placeholder=\"Type your email\"> 
                                    <input type=\"submit\" value=\"Subscribe\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-8\">
                            <div class=\"copyright text-white text-center\">
                                <p>&copy; Copyright by JohnAbbott LibraryAW 2019.</p>
                            </div>
                        </div>

                        <div class=\"col-md-4\">
                            <div class=\"footer-card-icon\">
                                <i class=\"fab fa-cc-discover\"></i>
                                <i class=\"fab fa-cc-mastercard\"></i>
                                <i class=\"fab fa-cc-paypal\"></i>
                                <i class=\"fab fa-cc-visa\"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>

", "master.html.twig", "C:\\xampp\\htdocs\\ipd17\\library-project\\templates\\master.html.twig");
    }
}
