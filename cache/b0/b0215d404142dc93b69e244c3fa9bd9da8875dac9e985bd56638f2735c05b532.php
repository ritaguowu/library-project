<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /register_user/register_user_master.html.twig */
class __TwigTemplate_7e73fbae3adf06b87d7f3267283e81fe11e24317e3e49e50b57eaa56513bc629 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'headAdd' => [$this, 'block_headAdd'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("master.html.twig", "/register_user/register_user_master.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        // line 4
        echo "    Register User Homepage
";
    }

    // line 7
    public function block_headAdd($context, array $blocks = [])
    {
        // line 8
        echo "    <script>
        \$(document).ready(function () {
            \$('.optionBt').click(function () {
                var name = \$(this).attr('name');
                \$(\"#content-detail\").load(\"/registerUser/\" + name);
            });

        });

    </script>

";
    }

    // line 21
    public function block_content($context, array $blocks = [])
    {
        // line 22
        echo "
    <div class=\"row\">
        <div class=\"col-4\">
            <!-- Sidebar -->
            <div class=\"row\">
                <div class=\"col-1\"></div>
                <div class=\"col-11\">
                    <p></p>
                    <p class=\"h2\"> My Account </p>
                    <!--Collpase-->
                    <div class=\"accordion\" id=\"accordionExample\">
                        <div class=\"card\" >
                            <div class=\"card-header\" >
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\"  name=\"welcome\">
                                        Welcome
                                    </button>
                                </h2>
                            </div>
                        </div>
                        <div class=\"card\">
                            <div class=\"card-header\" >
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\" name=\"loanRenew\" >
                                        Loans and Renewals
                                    </button></h2>
                            </div>
                            <div  data-parent=\"#accordionExample\">
                                <div class=\"card-body\">
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Number of loans</div>
                                        <div class=\"col-2\">";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "loan_numb", []), "html", null, true);
        echo "</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Items overdue</div>
                                        <div class=\"col-2\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "overdue_numb", []), "html", null, true);
        echo "</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Items soon overdue</div>
                                        <div class=\"col-2\">";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "soondue_numb", []), "html", null, true);
        echo "</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"card\">
                            <div class=\"card-header\" id=\"headingTwo\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\" name=\"loanHistory\">
                                        Loan history
                                    </button>
                                </h2>
                            </div>
                            <div  data-parent=\"#accordionExample\">
                                <div class=\"card-body\">

                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">payments due</div>
                                        <div class=\"col-2\">";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "pay_due", []), "html", null, true);
        echo "</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                       <div class=\"card\" id=\"headingThree\">
                            <div class=\"card-header\" id=\"headingThree\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\" name=\"payment\" >
                                        Payment history
                                    </button>
                                </h2>
                            </div>
                        </div>
                        <div class=\"card\" id=\"headingFour\">
                            <div class=\"card-header\" id=\"headingFour\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\"  name=\"reserve\">
                                        Reservations
                                    </button>
                                </h2>
                            </div>
                            <div  data-parent=\"#accordionExample\">
                                <div class=\"card-body\">

                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Number of reservations</div>
                                        <div class=\"col-2\">";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "reserv_times", []), "html", null, true);
        echo "</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Available resrvations</div>
                                        <div class=\"col-2\">";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), "reserv_times", []), "html", null, true);
        echo "</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"card\" id=\"headingFive\">
                            <div class=\"card-header\" id=\"headingFive\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2\" data-toggle=\"collapse\" data-target=\"#collapseFive\" aria-expanded=\"true\" aria-controls=\"collapseFive\">
                                        Personal Information
                                    </button>
                                </h2>
                            </div>
                            <div id=\"collapseFive\" class=\"collapse\"  aria-labelledby=\"headingFive\" data-parent=\"#accordionExample\">
                                <div class=\"card-body\">

                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\"><a href=\"#\">Change my password</a></div>

                                    </div>
                                    <p>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "firstName", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "lastName", []), "html", null, true);
        echo "</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">";
        // line 147
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "address", []), "html", null, true);
        echo "</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">";
        // line 151
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", []), "html", null, true);
        echo "</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">Membership expiry Date:";
        // line 155
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "expiryDate", []), "html", null, true);
        echo "</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>



        <div class=\"col-8 bookListControl\">
            <div id=\"content-detail\">
                <h1 class=\"mt-4\">Welcome</h1>
                <hr>
                Hello <span class=\"font-weight-bold\">";
        // line 172
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "firstName", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "lastName", []), "html", null, true);
        echo "</span>
                <p></p>
                <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>. The top navbar is optional, and just for demonstration. Just create an element with the <code>#menu-toggle</code> ID which will toggle the menu when clicked.</p>
                <ui>
                    <li ><span class=\"font-weight-bold\">Loans and Renewals</span> : view a list of your loans, their due dates, and renew items</li>
                    <li><span class=\"font-weight-bold\">Loan history</span> : a list of all borrowed items (must be activated by staff)</li>
                    <li><span class=\"font-weight-bold\">Reservations</span> : view and/or cancel  your reservations</li>
                    <li><span class=\"font-weight-bold\">Personal information</span>: view your contact information and change your password</li>


            </div>
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "/register_user/register_user_master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 172,  234 => 155,  227 => 151,  220 => 147,  211 => 143,  183 => 118,  175 => 113,  143 => 84,  120 => 64,  112 => 59,  104 => 54,  70 => 22,  67 => 21,  52 => 8,  49 => 7,  44 => 4,  41 => 3,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}
    Register User Homepage
{% endblock %}

{%block headAdd %}
    <script>
        \$(document).ready(function () {
            \$('.optionBt').click(function () {
                var name = \$(this).attr('name');
                \$(\"#content-detail\").load(\"/registerUser/\" + name);
            });

        });

    </script>

{% endblock %}

{% block content %}

    <div class=\"row\">
        <div class=\"col-4\">
            <!-- Sidebar -->
            <div class=\"row\">
                <div class=\"col-1\"></div>
                <div class=\"col-11\">
                    <p></p>
                    <p class=\"h2\"> My Account </p>
                    <!--Collpase-->
                    <div class=\"accordion\" id=\"accordionExample\">
                        <div class=\"card\" >
                            <div class=\"card-header\" >
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\"  name=\"welcome\">
                                        Welcome
                                    </button>
                                </h2>
                            </div>
                        </div>
                        <div class=\"card\">
                            <div class=\"card-header\" >
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\" name=\"loanRenew\" >
                                        Loans and Renewals
                                    </button></h2>
                            </div>
                            <div  data-parent=\"#accordionExample\">
                                <div class=\"card-body\">
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Number of loans</div>
                                        <div class=\"col-2\">{{count.loan_numb}}</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Items overdue</div>
                                        <div class=\"col-2\">{{count.overdue_numb}}</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Items soon overdue</div>
                                        <div class=\"col-2\">{{count.soondue_numb}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"card\">
                            <div class=\"card-header\" id=\"headingTwo\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\" name=\"loanHistory\">
                                        Loan history
                                    </button>
                                </h2>
                            </div>
                            <div  data-parent=\"#accordionExample\">
                                <div class=\"card-body\">

                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">payments due</div>
                                        <div class=\"col-2\">{{count.pay_due}}</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                       <div class=\"card\" id=\"headingThree\">
                            <div class=\"card-header\" id=\"headingThree\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\" name=\"payment\" >
                                        Payment history
                                    </button>
                                </h2>
                            </div>
                        </div>
                        <div class=\"card\" id=\"headingFour\">
                            <div class=\"card-header\" id=\"headingFour\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2 optionBt\"  name=\"reserve\">
                                        Reservations
                                    </button>
                                </h2>
                            </div>
                            <div  data-parent=\"#accordionExample\">
                                <div class=\"card-body\">

                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Number of reservations</div>
                                        <div class=\"col-2\">{{count.reserv_times}}</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-9\">Available resrvations</div>
                                        <div class=\"col-2\">{{count.reserv_times}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=\"card\" id=\"headingFive\">
                            <div class=\"card-header\" id=\"headingFive\">
                                <h2 class=\"mb-0\">
                                    <button class=\"btn btn-link h2\" data-toggle=\"collapse\" data-target=\"#collapseFive\" aria-expanded=\"true\" aria-controls=\"collapseFive\">
                                        Personal Information
                                    </button>
                                </h2>
                            </div>
                            <div id=\"collapseFive\" class=\"collapse\"  aria-labelledby=\"headingFive\" data-parent=\"#accordionExample\">
                                <div class=\"card-body\">

                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\"><a href=\"#\">Change my password</a></div>

                                    </div>
                                    <p>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">{{v.firstName}} {{v.lastName}}</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">{{v.address}}</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">{{v.email}}</div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-1\"></div>
                                        <div class=\"col-11\">Membership expiry Date:{{v.expiryDate}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>



        <div class=\"col-8 bookListControl\">
            <div id=\"content-detail\">
                <h1 class=\"mt-4\">Welcome</h1>
                <hr>
                Hello <span class=\"font-weight-bold\">{{v.firstName}} {{v.lastName}}</span>
                <p></p>
                <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>. The top navbar is optional, and just for demonstration. Just create an element with the <code>#menu-toggle</code> ID which will toggle the menu when clicked.</p>
                <ui>
                    <li ><span class=\"font-weight-bold\">Loans and Renewals</span> : view a list of your loans, their due dates, and renew items</li>
                    <li><span class=\"font-weight-bold\">Loan history</span> : a list of all borrowed items (must be activated by staff)</li>
                    <li><span class=\"font-weight-bold\">Reservations</span> : view and/or cancel  your reservations</li>
                    <li><span class=\"font-weight-bold\">Personal information</span>: view your contact information and change your password</li>


            </div>
        </div>

    </div>

{% endblock %}", "/register_user/register_user_master.html.twig", "C:\\xampp\\htdocs\\ipd17\\library-project\\templates\\register_user\\register_user_master.html.twig");
    }
}
